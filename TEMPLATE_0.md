// USE ONE FROM lifelib-to-calculang



```js
// wrap echoed source code by details tag; hot reload breaks this but ok for now
document.querySelectorAll('.observablehq-pre-container:has(> pre[data-language="js"])').forEach(el => {
  let wrapper = document.createElement('details');
  wrapper.className = 'code'
  let summary = document.createElement('summary')
  summary.textContent = "code 👀"
  wrapper.appendChild(summary)
  el.parentNode.insertBefore(wrapper, el);
  wrapper.appendChild(el);
});

//document.getElementById('wrapper').insertBefore(document.getElementById('lhs'),document.getElementById('content'));

/*document.querySelectorAll('div.observablehq--block').forEach(div => {
  if (div.innerHTML === '') div.remove();
})*/

// above dom manips all cause new els on hot reload..
```



```js
const introspection = await getIntrospection('entrypoint.cul.js', fs)

const compiled = await compile_new('entrypoint.cul.js', fs, introspection) // ?
const esm = compiled[0]
//display(introspection.cul_input_map) todo put under devtools
//display(introspection)

// these need more thought in a modular world
// or i am overthinking it
const inputs = [...introspection.cul_functions.values()].filter(d => d.reason == 'input definition' /* Breaks show demand curve && d.cul_scope_id == 0*/).map(d => d.name).sort()

//display(Object.values(introspection.cul_functions))
//display([...introspection.cul_functions.values()])

const formulae = [...introspection.cul_functions.values()].filter(d => d.reason == 'definition').map(d => d.name)


const formulae_not_inputs = [...introspection.cul_functions.values()].filter(d => d.reason == 'definition' && inputs.indexOf(d.name+'_in') == -1).map(d => d.name)
//display(formulae_not_inputs)


const bundle = bundleIntoOne(compiled, introspection, true)


const u = URL.createObjectURL(new Blob([bundle], { type: "text/javascript" }))
console.log(`creating ${u}`)

const model = await import(u)


//display(Object.assign(document.createElement('div'), {className: 'hide-empty-block'}))

invalidation.then(() => {console.log(`revoking ${u}`); URL.revokeObjectURL(u)});

```


```js

import { calcuvizspec, function_inputs_table, graph_functions, calcudata } from "https://cdn.jsdelivr.net/gh/declann/calculang-js-browser-dev@55c8ed1/helpers.js"

import { introspection as getIntrospection, compile_new, bundleIntoOne, calls_fromDefinition  } from "https://cdn.jsdelivr.net/gh/declann/calculang-js-browser-dev@55c8ed1/calculang.js" // PINNED VERSION (due to cache)

import embed from 'npm:vega-embed';

//display(Object.assign(document.createElement('div'), {className: 'hide-empty-block'}))

```