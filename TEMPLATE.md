<!-- SPDX-License-Identifier: AGPL-3.0-only -->
<!-- Declan Naughton -->


<!--
TEMPLATE.md provides:
- collabsible code blocks
- editor
- JS bits: model (obv), calcuvegadata, embed, compile etc.
  -->

<div style="opacity:0.1">

```js
const cul_scope_id = view(Inputs.range([0,10], {label:'cul_scope_id', value:0, step:1}))
```

</div>


```js
const scrollIntoViewOpts = {behaviour:'smooth', block:'center'}

```



```js
display(html`<style>

.fui-FluentProvider{
  display: none;
}

.vega-embed {
  width: 100%;
  height: 100%;
}
  
  .answer {
    color: blue !important;
    font-weight: bold;
    font-size: 1.5em !important;
    left: 80px !important;
    top: 10px;
}
  .answer .tooltiptext  {
    /*border: 2px dashed black;*/
    background: #af5a;
    padding: 10px;
    border-radius: 15px;
  
  }

  .answer .tooltiptext::after {
    /*color: red;*/
    border: none;
  }
  
.answer:hover {
  /*font-size:2em !important;*/
}
  
  body {
  font-family: system-ui;
}

pre {
  line-height: 25px;
}

.calculang_tooltip {
  color: #19f;
  position: relative;

}

.tooltiptext {
  position: absolute;
  bottom: calc(100% - 15px);
  left: anchor(center);
  transform: translateX(-50%);
  width: max-content;
}

.tooltiptext {
  cursor: zoom-in;
}


.calculang_tooltip.answer .tooltiptext {
  bottom: 0;
} 

calculang_tooltip.answer {
    background-color: lightorange;
}


pre {
  cursor:default
}
  </style>
  `)

display(html`<style>

.inputs {
  padding: 20px;
}

button.btn {
  background-color: antiquewhite;
  color: blue;
  padding:0;
}

.calculang {
  line-height: 40px;
    -webkit-transition: all 0.2s linear;
    -moz-transition: all 0.2s linear;
    -o-transition: all 0.2s linear;
    transition: all 0.2s linear;
  
}
div.calculang.cm-line:has(span.calculang_title) {
  margin-top:20px;
}

.calculang.calculang_overwritten {
  text-decoration: line-through;
  font-weight: bold;
}

${formulae_hidden.length ? `.calculang {
    background: #aaf3;
}` : ''}


${formulae_hidden.map(d => `.calculang_f_${d} + .calculang_f_${d}`).join(', ')} {
  filter: blur(7px);
  line-height: 5px;
  opacity: 0.5;
} /* + selector to exclude first! https://coderwall.com/p/uugurw/css-selector-trick-select-all-elements-except-the-first */
${formulae_hidden.map(d => `.calculang_f_${d}`).join(', ')} {
  opacity: 0.5;
  background: white;
}${formulae_hidden.map(d => `.calculang_f_${d} .calculang_tooltip`).join(', ')} {
  opacity: 0.1;
  background: white;
}
${formulae_hidden.map(d => `.calculang_f_${d} > .calculang_title`).join(', ')} {
  background-color: lightgreen;
  font-size: 0.5em;
}

span.calculang_call { 
  white-space:nowrap;
  cursor: zoom-in;
  background-color: lightblue;
}

.tooltiptext {/* not all tooltiptexts go under a calculang call span! (single-length ones dont) */
  /*color: red;*/
  font-weight: bold;

}

.calculang_call.calculang_call_input {
  /*cursor: none;*/
  background-color: pink;
}

.calculang_call.calculang_call_input .tooltiptext {
  color: #f53 !important;;
}

.calculang .calculang_title span {
  padding: 3px;
  background: #af54;
  font-size: 1.5em;
  font-weight: bold;
  border: 1px solid grey;
    -webkit-transition: all 0.2s linear;
    -moz-transition: all 0.2s linear;
    -o-transition: all 0.2s linear;
    transition: all 0.2s linear;

}

.calculang_title.calculang_title_renamed {
  text-decoration: line-through;
  text-decoration-style: dashed;
}
</style>
`)
```

```js
import { SourceMapConsumer } from 'npm:source-map-js';

const map = new SourceMapConsumer(esm.map);

//const esm = compiled[0]


const maps = compiled.map(d => d.map)
const mapsc = maps.map(d => new SourceMapConsumer(d))


//const cul_scope_id = 0
//debugger;
const calls_annotations = 
  calls_fromDefinition(introspection).filter(d => d.reason == 'call' && d.from != '0_undefined')
  .map (d => ({...d, cul_scope_id: +d.from/*from/to are consistent due to bringing everything into common scope*/.split('_')[0]}))
        .map((d) => ({
        ...d,
        mjs_loc: {
          start: mapsc[d.cul_scope_id].generatedPositionFor({
            ...d.loc.start,
            source: maps[d.cul_scope_id].sources[0] //`${only_entrypoint_no_cul_js}-nomemo.cul.js` // todo update !
          }),
          end: mapsc[d.cul_scope_id].generatedPositionFor({
            ...d.loc.end,
            source: maps[d.cul_scope_id].sources[0] //`${only_entrypoint_no_cul_js}-nomemo.cul.js`
          })
        }
      }))
      .map((d) => ({
        ...d,
        mjs: compiled[d.cul_scope_id].code
          .split("\n")
        [d.mjs_loc.start.line - 1].substring(
          d.mjs_loc.start.column,
          d.mjs_loc.end.column
        )
      })) // assuming completely on one line
      .map(d => ({...d, mjs: d.mjs.slice(-1) == ')' ? d.mjs : d.mjs+')'}))/*issue for simple-loan final call for some reason??*/
      .map(d => {
        if (!(CONFIG.USE_HIGHLIGHTING || 0)) return d;
        //return d; I did stop this for some reason? DOESNT WORK FOR LIFE
  const selection_fn = new Function("model", "{"+Object.keys(cursor).join(",")+"}", `Object.assign(window, model); try { return ({value:${d.mjs}, cursor: ${d.mjs.slice(d.from.length-2)}}) } catch(e) { console.error('trap2', e)}`) // using hacky way to get cursor, for calculang-at-fosdem I used babel: `is` function
  return {...d, ...selection_fn(model, cursor)} // try important due to cursor often not setup initially (e.g. awaiting)
})
```

```js

//debugger
const fns_annotations = [...introspection.cul_functions.values()].filter(
        (d) =>
          d.reason == "definition" || d.reason == 'definition (renamed)' /*&&
          d.cul_scope_id == cul_scope_id*/
      )
      .map(d => {
        //if (d.name == 'leftness') debugger;
        //debugger;
        const dd = {...d}
        //debugger;
        dd.inputs = [...introspection.cul_input_map.get(d.cul_scope_id+'_'+d.name)]

        const selection_fn = new Function("model", "{"+dd.inputs.join(",")+"}", `Object.assign(window, model); try { return s${dd.cul_scope_id}_${d.name}({${dd.inputs.join(",")}}) } catch(e) { console.error('trap', e) }`)

        dd.v = selection_fn(model, cursor)

        return dd //return selection_fn(model, c)
      })
```

```js



const selection_start = map.generatedPositionFor({...selection.from, source:"unknown"})
const selection_end = map.generatedPositionFor({...selection.to, source:"unknown"})

const esm_split = esm.code.split('\n')
```

```js
const formulae_all = [...introspection.cul_functions.values()].filter(d => d.reason == 'definition').map(d => d.name)

const formulae_hidden = formulae_all.filter((d) => !formulae_visible.includes(d))


```

<div id="wrapperwrapper">
<div id="wrapper" class="wrapper sticky">
<div id="lhs" class="lhs side">
  <div class="grow">
  <h1>ƒ</h1>
  <details class="calculang" open><summary class="calculang" style="margin-bottom:10px">calculang ✍️</summary>
  <span style="font-style: italic">editable and dangerous!</span> 🧙‍♂️⚠️

```js


import {editor as editorCm, config as eslintConfig} from './components/editor/editor.bundle.js'

import {calcuvegadata} from './components/calcuvegadata.js'


const fs = Mutable(cul_default)

function set_fs(fs2) {
  fs.value = ({...fs2});
}

const filename = Mutable('entrypoint.cul.js') // separate?
function set_filename(n) {
  filename.value = n;
}

const inhibit = Mutable(false) // inhibit update fs if only a setScope

function setInhibit(d) {
  inhibit.value = d;
}

const selection = Mutable({from:{line:1,column:1}, to:{line:1,column:2}})


const formulae_visible = Mutable([]); // chg to all ? formulae_all
const hover = Mutable(""); // chg to all ? formulae_all

function set_formulae_visible (d) {
  formulae_visible.value = [...d]
}

function set_hover (d) {
  hover.value = d
}

//debugger;

console.log('AGAIN!!')


const editor = await editorCm({/*parent: document.getElementById('editor_placeholder'),*/ doc: cul_default['entrypoint.cul.js']/*fs['entrypoint.cul.js']*/, update: update => { if(0 && inhibit.value) setInhibit(false); else
            fs.value = ({...fs.value, [editor.filename]: update.state.doc.toString() })

}, updateSelection: s => {selection.value = s}, set_formulae_visible
, set_hover, setCursor, setFormula
});
```

```js
display(editor)
```


  <details><summary>dev tools 🧰</summary>

```js
display(inputs_ui)
```


```js
import * as marked from 'npm:marked'

const e = document.createElement('div')
e.innerHTML = 'TODO bring back function_inputs_table'// marked.parse(function_inputs_table(introspection))

display(e)

//function_inputs_table(introspection)
```


```js
// for F9, not sure if I want to include? Or replace with optional markers?
// Or put in a panel?
// Do selection eval? (like in tixy)
/*import * as lineColumn from 'npm:line-column';
const index_start =lineColumn.default(esm.code).toIndex(selection_start)
const index_end =lineColumn.default(esm.code).toIndex(selection_end)
*/
const selection_esm = ""//esm.code.slice(index_start+1,index_end+1)

```

#### select-F9 🧙‍♂️ javascript

<pre style="font-size:0.5em; scrollbar-width: thin">
${selection_esm}
</pre>

```js
const cursor0 = Mutable({}) // every input should be in, but with what values?

const setCursor = (k,v) => {
  //if (k == 'ray_angle_in') debugger;
  //if (k == 'keys_stream_function_in') debugger;
  cursor0.value = {...cursor0.value, [k]:v};
}
```

```js
const formula = Mutable("")

const setFormula = (v) => {
  formula.value = v
}
```

introspection:

```js
introspection
```

<details><summary>reactive workings</summary>

```js
display(calls_annotations)
display(fns_annotations.map(d => d.v))
```

</details>

  </details>

<details><summary style="font-size:1em">complete javascript ✨</summary>
  <span style="font-style: italic">generated from calculang</span> ⬆️
  ${view(Inputs.textarea({value:esm.code, rows:60, resize: true, disabled:true}))}
  </details>

<details><summary>🤫</summary>

```js
const fmt_fmt = view(Inputs.text({value:",.2f", label: 'fallback format string'}))
const actions = view(Inputs.toggle({label:'vega actions', value: false}))
```

</details>

  </details>


  </div>
</div>

<div style="display:none">


<!-- @include: /home/declan/repos/next-calculang-gallery/TEMPLATE_0.md -->


REACTIVE FORMULAS:

```js
// page needs to provide excludes
const dependencies = inputs.filter(d => !excludes.includes(d))


```

```js

// todo fmt hints or templates? like a type or unit?
// is this a big bottleneck?
const fmt2 = (name, value) => {
  name = name?.toLowerCase();
  if (name == 'emissions_my_lifetime_proportion') return d3.format(',.2%')(value)
  if (name == 'x' || name == 'y') return d3.format(',.4f')(value)
  if (name == "map") return "[[..]]"
  if (Array.isArray(value)) return "[..]"
  /*if (modelname == 'fizzbuzz') return typeof value == 'number' ? d3.format(',.0f')(value) : value // works well for fizzbuzz
      if (name == 'now' || name == 'date') return d3.timeFormat("%Y-%m-%d")(value)

  if (modelname == 'emissions_my_lifetime') {
    if (name == 'now') return d3.timeFormat("%Y-%m-%d")(value)
    if (name == "emissions_my_lifetime_proportion") return d3.format(".3%")(value)
    if (typeof value == 'number')
    return d3.format(".5k")(value)
  }*/
  if (name == "v") return d3.format(",.5f")(value)
  if (name == "v_pow_term_left") return d3.format(",.5f")(value)
  if (name.includes("_hit_object")) return "{..}"//`{x: ${value.x.toFixed(2)}, y: ${value.y.toFixed(2)}, v: ${value.v}, step_in: ${value.step_in}}`
  if (name.includes("_co")) return d3.format(",.0f")(value)
  if (name.includes("rate") && name.includes("interest")) return d3.format(".3%")(value)
  if (name.includes("rate") || name.includes("percent") || name.includes("fraction")) return d3.format(".2%")(value)
  if ((name.includes("year")) || name.includes("term") || name.includes("duration") || name.includes("frame")) return d3.format(".0f")(value)
  if (name.includes("object") || typeof value != 'number') return JSON.stringify(value)

  return d3.format(fmt_fmt)(value)
}

```

```js
/*cul_scope_id;
console.log('CUL SCOPE ID', cul_scope_id); // 
*/

// DOM not updated at this point in scope change !!
fns_annotations.forEach((d,i) => {
  // error => breaks follow ups
  if (0) { // TEMP Answers instead of tooltips
  if (document.getElementById(d.name)) // TODO replace with fmt2?
  document.getElementById(d.name).innerHTML = JSON.stringify(d.v) /*d3.format(',.2f')(d.v)*/ + "<div class='cm-tooltip-arrow' />"
  }
  else {
    //debugger;
    let e = document.getElementById('a-'+i)
    if (e)
    e.textContent = fmt2(d.name, d.v)//fmt(v);
  }
});
```

```js
window.calls_annotations = calls_annotations;

calls_annotations.forEach((d, i) => {
  // error => breaks follow ups
  if (document.getElementById('w-'+i))
  document.getElementById('w-'+i).textContent = fmt2(d.from.split('_').slice(1).join('_'), d.value)//JSON.stringify(d.v) /*d3.format(',.2f')(d.v)*/ + "<div class='cm-tooltip-arrow' />"
});
```

REACTIVE INPUTS:


```js
import {up} from './components/reactive-inputs2.js'
```



```js
document.querySelectorAll('.inputs .flash').forEach(d => d.remove())
inputs_ui.dataset.inputs = '' // up ran first? so use fake to control order:
const fake = ''
```

```js
const inputs_ui = document.createElement('div')
inputs_ui.className = 'inputs'
```

```js
console.log("don't repeat")

const extra_cursor_Input = Inputs.input({})
const extra_cursor = Generators.input(extra_cursor_Input)
```

```js
const cursor = ({...cursor0, ...extra_cursor})
```

```js
fake;
//extra_cursor; // need to react to changes to cursor
up(inputs_ui, extra_cursor_Input, dependencies, model, {}); // must keep minibinds sep. to pick up updates
// because needs to detect removal for flash (above)
```




</div>


