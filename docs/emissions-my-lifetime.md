---
title: emissions (WIP)
draft: false
---

This might fail locally, but that's because cors.

```js
const CONFIG = {USE_HIGHLIGHTING: true /* to show workings */}
// needed (empty ok)
const excludes = [] // exclusions for "reactive inputs" (under dev tools atm)
```

```js
setCursor('age_in',29);
setCursor('now_in',new Date(2019, 0, 1));
setCursor('timing_in',"start of birthyear");

```

```js
setFormula('emissions_my_lifetime_proportion');
set_formulae_visible(['emissions_my_lifetime_proportion'])
//setTimeout(() => {document.querySelector('.calculang_f_'+'emissions_my_lifetime_proportion').scrollIntoView(scrollIntoViewOpts)}, 100)
```

<!-- @include: /home/declan/repos/next-calculang-gallery/TEMPLATE.md -->

<div id="content">
  
# emissions my lifetime

TODO

- reconcile to results from [Your Personal Carbon History](https://parametric.press/issue-02/carbon-history/) (mainly different data sources and allowance for time/continuation?)
- visuals/interactions

<details><summary>important disclaimers ❗</summary>

⚠️ *The assumptions, methodology and limitations of a model should be carefully considered for any purpose you apply it to. I haven't even listed these here, nor otherwise documented the model fully/clearly.* **Proceed with caution** 🙏

⚠️ *This is not a complete model/narrative and you should not rely on it.*

⚠️ *Not recommended to enter real personal or sensitive details. **Contact me with real-world calculation or model requirements instead.*** 🗣✅

⚠️ *This validator/scenario tool isn't/won't be financial advice etc.*
</details>



```js echo
display(model.emissions_my_lifetime({ age_in: 33, now_in: new Date(), timing_in: "start of birthyear" }))

display(model.total_emissions({ now_in: new Date() }))

display(model.emissions_my_lifetime_proportion({ age_in: 33, now_in: new Date(), timing_in: "start of birthyear" }))
display(model.emissions({ age_in: 33, now_in: new Date(), timing_in: "start of birthyear", emissions_year_in: 2024 }))
display(model.emissions({ age_in: 33, now_in: new Date(), timing_in: "start of birthyear", emissions_year_in: 2023 }))
display(model.emissions({ age_in: 33, now_in: new Date(), timing_in: "start of birthyear", emissions_year_in: 2022 }))


display(model.emissions_my_lifetime_proportion({ age_in: 29, now_in: new Date(2019,0,1), timing_in: "start of birthyear" }))

```

</div>

</div></div><!-- close tag started in template 2x -->


```js
const cul_default = ({
  "entrypoint.cul.js": await FileAttachment("emissions-my-lifetime.cul.js").text(),
  "dummy": await FileAttachment("co2_data.mjs").text(),
  "dummy2": await FileAttachment("annual-co2-emissions-per-country.csv").text()

})
```






```js
// I CAN'T USE reactive width because hot reload will wipe it to 0
// https://github.com/observablehq/framework/issues/1194
const content_width = Generators.width(document.getElementById("content2")); // keep as a generator for reactivity

// circular definition if I use cul_default ?!
const cul_Input = Inputs.input(cul_default);
const cul = Generators.input(cul_Input);
```
