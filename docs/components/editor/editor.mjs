// SPDX-License-Identifier: AGPL-3.0-only
// Declan Naughton


// https://codemirror.net/examples/bundle/

// guided by node_modules/codemirror/dist/index.js

const cul_scope_id = 0; // variable at fosdem, had a view ui

const scrollIntoViewOpts = {behaviour:'smooth', block:'center'}

import {html} from "htl";

import { introspection as getIntrospection } from "https://cdn.jsdelivr.net/gh/declann/calculang-js-browser-dev@55c8ed1/calculang.js"



import { lineNumbers, highlightActiveLineGutter, highlightSpecialChars, highlightTrailingWhitespace, drawSelection, dropCursor, /*rectangularSelection, crosshairCursor,*/ highlightActiveLine, keymap, scrollPastEnd } from '@codemirror/view';
import { EditorView, ViewPlugin, Decoration, Tooltip, showTooltip, WidgetType } from '@codemirror/view';
import { EditorState, EditorSelection, StateField } from '@codemirror/state';
import { foldGutter, indentOnInput, syntaxHighlighting, defaultHighlightStyle, bracketMatching, foldKeymap, syntaxTree, foldAll } from '@codemirror/language';
import { history, defaultKeymap, historyKeymap } from '@codemirror/commands';
import { highlightSelectionMatches, searchKeymap } from '@codemirror/search';
import { closeBrackets, autocompletion, closeBracketsKeymap, completionKeymap } from '@codemirror/autocomplete';
import { lintKeymap } from '@codemirror/lint';

//import { introspection2 } from "../components/mini-calculang-rollup.js"

import { range } from 'underscore'

//import { EditorView, basicSetup } from "codemirror"
import { javascript, javascriptLanguage, esLint } from "@codemirror/lang-javascript"

/////// linting ///

// linting via eslint might be mad heavy??? a few calculang-specific things instead?
// example  https://github.com/UziTech/eslint-linter-browserify/blob/f0d475beb86e50df09e2db76e2770608c6e74f47/example/script.js
import { linter, lintGutter } from "@codemirror/lint";
import * as eslint from "eslint-linter-browserify";

export const config = {
  // eslint configuration
  parserOptions: {
    ecmaVersion: 2019, // ?
    sourceType: "module",
  },
  env: { // ?
    browser: true,
    node: true,
  },
  rules: {
    //semi: ["warn", "never"],
    "no-undef": ["error", "never"], // this is a key rule for calculang, but is easier to use on JS (because of intentional undefined inputs in _in convention)
    "func-style": ["error", "expression"],
    "consistent-return": ["error"], // not as good as forcing a return ..
    // no-unused-expressions works in some cases but not others?
    // bug whenever a = () => {b()}, but b()*3 works
    "no-unused-expressions": ["error"], // doesn't catch when there are calls because doesn't know about purity ..
    "prefer-const": "warn",
    'no-restricted-syntax': [ // docs https://eslint.org/docs/latest/rules/no-restricted-syntax
      'error',
      {
        message: "calculang: don't pollute the _ namespace",
        selector:
          'ImportDeclaration[source.value=/cul_scope_/] > ImportSpecifier[local.name=/_$/]',
        // converted to esm => match cul_scope_x rather than .cul
      }, // test with import {create as confetti_} from "https://cdn.jsdelivr.net/npm/canvas-confetti/+esm?cul_scope_id=3";

    ],

  },
  // "extends": "eslint:recommended" not working; not sure I want
};





// some custom stuff
import snippetbuilder from 'codemirror-6-snippetbuilder'

import jssnippetarray from './snippetarray.js'

import interact from '@replit/codemirror-interact';

// WISHLIST:
// debounce compilation !! CM has linter logic to execute linting on pause?
// clearup autocomplete
// REFACTORING: selection to formula (+replace other occurances)
// " selection wraped in dupe true conditional and calculang-patterns of this
// done highlight initially or in readonly mode?
// coloured brackets or something like it
// prettier/eslint integration (moreso eslint!)
// ~~tab behaviour~~ a11y https://codemirror.net/examples/tab/
// rename or select all on highlight a fn name


// calculang specific stuff
// on autocompletion to calculang f do () and {}?
// NAVIGATION WITH PROPER SCROLLTO/INTOVIEW
// use tooltips vs decorations & css positioning for overlays (which works)? https://codemirror.net/examples/tooltip/
// better decoration to deemphasize cruft

// going wild for a little with extensions
import { indentationMarkers } from '@replit/codemirror-indentation-markers'; // calculang is expression-heavy so I think this isn't useful

const readonly = false;
const decorations = true;

let introspection;// = introspection2(""); // it is crazy to run introspection here on each edit - but I can mitigate, and the real crazy thing is actually running the model (on WIP stuff which will fail)

// from calculang-at-fosdem

//////////// plugin 1: decoration & cul navigation for function-level

const formulaDecorations =
  (div, set_formulae_visible) => {
    //set(options.formulae_visible, [""]) // this was doing a lot of jank!!
    const formulaDecorations = ViewPlugin.fromClass(
      class {

        constructor(view) {
          this.decorations = formulaDecoration(div, view);
        }

        update(update) {
          if (update.docChanged || update.viewportChanged)
            this.decorations = formulaDecoration(div, update.view);
        }
      },
      {
        decorations: (v) => v.decorations,

        eventHandlers: {
          mousedown: (e, view) => { // HMMM I'll need to resolve that calls get precedence!!! Should be working:
            let classList = [...e.target.classList, ...e.target.parentElement.classList] // very open, be aware of bugs ?!
            if (e.target.classList.contains("cul_call")) // TODO
              1;//set(viewof formulae_shown, [target.textContent]); TODO
            else if (
              //e.target.classList.contains("deposits") ||
              //e.target.parentElement.classList.contains("deposits")
              classList.filter(d => d.includes("calculang_f_")).length
            )
              /////// at-fosdem: set(options.formulae_visible, [classList.find(d => d.includes("calculang_f_")).slice("calculang_f_".length)]);
              //debugger;
              // mitigate overlapping event fires (call and formula)
              if (classList.includes('calculang_call') || classList.includes('calculang_tooltip') || classList.includes('tooltiptext')) return;
              else set_formulae_visible([classList.find(d => d.includes("calculang_f_")).slice("calculang_f_".length)])
              // May 2024: maybe jitter below is/was due to overlapping event fires?
              /////// causes unnecessary jitter when we already see f document.querySelector('.'+classList.find(d => d.includes("calculang_f_"))).scrollIntoView(scrollIntoViewOpts)
              //trying it 
              //document.querySelector('.'+classList.find(d => d.includes("calculang_f_"))).scrollIntoView(scrollIntoViewOpts)


          }
        }
      }
    );

    return [formulaDecorations];
  }


// Decoration.lines
const formulaDecoration = (div, view) => Decoration.set(
  //formula_line_details
  //const formula_line_details =
  /////Object.values(/*calculang_source_*/introspection.cul_functions)
  [...introspection.cul_functions.values()]
  .filter((d) => d.reason.includes("definition") && d.name.slice(-3) != '_in' && d.cul_scope_id == div.scope)
  .map(({ name, loc, reason }) => ({
    name,
    lineStart: loc?.start.line,
    lineEnd: loc?.end.line,
    overwritten/*AND not imported => strikethrough*/: name.slice(-1) == '_' && !(/*calculang_source_*/[...introspection.cul_functions.values()].filter(d => d.imported == name && d.cul_source_scope_id == div.scope)).length // and not explicit imported/used in parent defn
  }))
    .map((d) =>
      range(d.lineStart, d.lineEnd + 0.1).map((e) =>
        Decoration.line({
          attributes: {
            class: "calculang" + " " + "calculang_f_"+d.name + (d.overwritten ? ' calculang_overwritten' : '') /* +style available */ // leaving scope and other potential conflicts out
          }
        }).range(view.state.doc.line(e).from)
      )
    )
    .flat())


// C&Ped above
// const formula_line_details = Object.values(/*calculang_source_*/introspection.cul_functions)
//   .filter((d) => d.reason.includes("definition") && d.name.slice(-3) != '_in' && d.cul_scope_id == cul_scope_id)
//   .map(({ name, loc, reason }) => ({
//     name,
//     lineStart: loc?.start.line,
//     lineEnd: loc?.end.line,
//     overwritten: name.slice(-1) == '_' && !(Object.values(/*calculang_source_*/introspection.cul_functions).filter(d => d.imported == name && d.cul_source_scope_id == cul_scope_id)).length // and not explicit imported/used in parent defn
//   }))

// const formula_line_details_0 = Object.values(/*calculang_source_*/introspection.cul_functions)
//   .filter((d) => d.reason.includes("definition") && d.name.slice(-3) != '_in' && d.cul_scope_id == 0)
//   .map(({ name, loc, reason }) => ({
//     name,
//     lineStart: loc?.start.line,
//     lineEnd: loc?.end.line,
//     overwritten: name.slice(-1) == '_' && !(Object.values(/*calculang_source_*/introspection.cul_functions).filter(d => d.imported == name)).length // and not explicit imported/used in parent defn
//   }))



// from calculang-at-fosdem
///////////////////////////
let calculang_identifier_decorations = (set_formulae_visible, set_hover, div) => {
  return ViewPlugin.fromClass(class {
    constructor(view) {
      //introspection = introspection2(view.state.doc.toString())
      //introspection = await getIntrospection("entrypoint.cul.js", div.fs)
      
      this.decorations = identifier_decorations(view);
    }

    async update(u) {
      div.fs = ({...div.fs, [div.filename]: u.state.doc.toString() })
      console.log('calculang_identifier_decorations UPDATE HAPPENED')
      let introspection_new
      try { // without a try inevitable errors break plugin (though should be off when not readonly !)
        // TODO wrap in readonly
        introspection_new = await getIntrospection("entrypoint.cul.js", div.fs)
        //introspection_new = introspection2(u.state.doc.toString())
        //console.log('TTT', introspection2(u.state.doc.toString()))
      } catch (e) { introspection_new = introspection }
      introspection = introspection_new
      if (1 || u.docChanged || u.viewportChanged) // I need to capture fold changes too !
        this.decorations = identifier_decorations(u.view);
      /*if (1 || update.docChanged) {
        this.decorations = identifier_decorations(view);
        this.dom.textContent = update.state.doc.length
      }*/
    }

    destroy() { this.dom.remove() }
  },
    {
      decorations: (v) => v.decorations,



      eventHandlers: {
        mousemove: (e, view) => {
          let classList = [...e.target.classList, ...e.target.parentElement.classList] // very open, be aware of bugs ?!
                      let c = classList.filter(d => d.includes("calculang_call_"))
          let cc= 'badvalue';
          if (c.length) cc = c[0].slice("calculang_call_".length)

          if (e.target.classList.contains("calculang_call") || classList.includes("calculang_title")) { // TODO only if the containing formula is visible, I have options.formulae_visible:
          //console.log('HIHIHIEE')
          //debugger;
            //set(viewof hover, cc) // too many triggers !
            set_hover(cc)
            
          }
        },
        mousedown: (e, view) => {
          let classList = [...e.target.classList, ...e.target.parentElement.classList] // very open, be aware of bugs ?!
                      let c = classList.filter(d => d.includes("calculang_call_") && d != "calculang_call_input")
          let cc= 'badvalue';
          if (c.length) cc = c[0].slice("calculang_call_".length)

          if (e.target.classList.contains("calculang_call") || classList.includes("calculang_title")) { // TODO only if the containing formula is visible, I have options.formulae_visible:
          //console.log('HIHIHIEE')
          //debugger;
            //set(options.formulae_visible, [cc])
            //debugger;
            set_formulae_visible([cc])
            document.querySelector('.calculang_f_'+cc).scrollIntoView(scrollIntoViewOpts)
          }

          else if (
            //e.target.classList.contains("deposits") ||
            //e.target.parentElement.classList.contains("deposits")
            classList.filter(d => d.includes("calculang_f_")).length
          ) { // this is setting when you click in the area of a formula - no need to scroll
            //set(options.formulae_visible, [classList.find(d => d.includes("calculang_f_")).slice("calculang_f_".length)]);
            set_formulae_visible([classList.find(d => d.includes("calculang_f_")).slice("calculang_f_".length)]);
            //document.querySelector('.'+classList.find(d => d.includes("calculang_f_"))).scrollIntoView(scrollIntoViewOpts)
          }
        }
      }


      
    }
  )
}

let formulae_all

// Decoration.marks specified by ranges
const identifier_decorations = view => {
  let decorations = [];
  //introspection = introspection2(view.state.doc.toString())
  const inputs = [...introspection.cul_functions.values()].filter(d => d.reason == 'input definition').map(d => d.name).sort()
  try {
    formulae_all = [...introspection.cul_functions.values()].filter(d => d.reason == 'definition' && inputs.indexOf(d.name/*+'_in'*/) == -1).map(d => d.name)//["line", "result", "wave","semi_circle", "x", "n", "radius"]
  } catch (e) { }

  //for (let { from, to } of view.visibleRanges) {
  console.log(view.state)
  console.log(view.visibleRanges)
  //for (let { from, to } of [{from: 0, to: 1000}]) {
  //debugger;
  //debugger;
  for (let { from, to } of view.visibleRanges) {
    //for (let { from, to } of [{from:0, to:5000}]) {
    //cmImports.language.syntaxTreeAvailable(view.state, )
    //cmImports.language.forceParsing(view, )

    syntaxTree(view.state).iterate({
      from,
      to,
      enter: (node) => {
        if (node.name == "VariableDefinition") {
          let name = view.state.doc.sliceString(node.from, node.to);
          if (formulae_all.includes(name)) {
            //debugger
            decorations.push(
              Decoration.mark({ class: "calculang_title" + (inputs.includes(name + "_in") ? " calculang_title_input" : "") }).range(
                node.from,
                node.to
              )
            );
          }
          if (formulae_all.includes(name + '_')) {
            decorations.push(
              Decoration.mark({ class: "calculang_title calculang_title_renamed" }).range(
                node.from,
                node.to
              )
            );
          }
        }

        if (node.name == "VariableName") {
          let name = view.state.doc.sliceString(node.from, node.to);
          if (formulae_all.includes(name)) {
            if (inputs.includes(name + "_in"))
              decorations.push(
                Decoration.mark({ class: `calculang_call calculang_call_input calculang_call_${name}` }).range(
                  node.from,
                  node.to
                )
              );
            else
              decorations.push(
                Decoration.mark({ class: `calculang_call calculang_call_${name}` }).range(node.from, node.to)
              );
          }
        }
      }
    });
  }
  return Decoration.set(decorations);
}
///////////////////////////


////////// from -fosdem



// ex- calls_with_mjs, relates to that in generator
//calls = calculang_source_introspection.cul_links
  //.filter(d => !d.from.includes('undefined')).filter((d) => d.reason == "call" && cul_scope_id == +d.to.split("_")[0]) // bugs for double digit scopes


// ex CheckboxWidget
class AnswersWorkingsOverlayWidget extends WidgetType {
  constructor(div, v, call, call_i, type, set_formulae_visible, setCursor, setFormula) {
    //a = v;
    super();
    this.div = div;
    this.v = v;
    this.call = call;
    this.call_i = call_i;
    this.type = type;
    this.set_formulae_visible = set_formulae_visible;
    this.setCursor = setCursor;
    this.setFormula = setFormula;
    //this.evals = type == "definition" ? mjs_q_eval : fns_with_mjs_q_eval;
    //this.info = type == "definition" ? fns_with_mjs : calls_with_mjs_qualified;
  }

  toDOM() {
    // mjs_q_eval; use this to test flicker/parse issues for plugins setting values (now set setparately by DOM calls)
    let wrap = document.createElement("span");
    //wrap.className = "tooltip";
    //wrap.textContent = "101.19";
    wrap.setAttribute("aria-hidden", "true"); // not in a good place, todo fix
    wrap.className = "calculang_tooltip";
    if (this.type == "definition") {
      return html`<span class="calculang_tooltip answer" id="c-a-${this.v}"><!--<input type="checkbox"></input>--><span id="a-${this.v}" class="tooltiptext" anchor="c-a-${this.v}">${99999/*fmt2(
        window.fns_with_mjs_q_eval[modelname][cul_scope_id][this.v].name, window.fns_with_mjs_q_eval[modelname][cul_scope_id][this.v].value//999
      )*/}</span></span>`;
    }
    return html`<span onclick=${() => {
      this.div.setScope(+window.calls_annotations[this.v].fromDefinition.split('_')[0])
      this.setFormula(window.calls_annotations[this.v].fromDefinition.split('_').slice(1).join('_'))
      this.set_formulae_visible([window.calls_annotations[this.v].from.split('_').slice(1).join('_')])
      document.querySelector('.calculang_f_'+window.calls_annotations[this.v].fromDefinition.split('_').slice(1).join('_')).scrollIntoView(scrollIntoViewOpts)
      //console.log(window.calls_annotations[this.v])
      //console.log(this.set_formulae_visible, this.setCursor)
      Object.entries(window.calls_annotations[this.v].cursor).forEach(([k,v]) => {
        this.setCursor(k, v)
      })
      // ex click handler
      /* TODO*/ //set(viewof formulae_visible, [ // I'm supposed to use options.formulae_visible !!
        //calls[this.v].from.slice(2)
      //]);
      //document.querySelector('.calculang_f_'+calls[this.v].from.slice(2)/*BUG for >9 scopes!*/).scrollIntoView(scrollIntoViewOpts)
      //if (window.mjs_q_eval[modelname][cul_scope_id][this.v].handler) window.mjs_q_eval[modelname][cul_scope_id][this.v].handler();

    }} class="calculang_tooltip" id="c-w-${this.call_i}"><!--&nbsp;<input type="checkbox"></input>--><span id="w-${this.call_i}" class="tooltiptext" anchor="c-w-${this.call_i}">${'⌛'/*fmt2( window.mjs_q_eval[modelname][cul_scope_id][this.call_i].name,
     window.mjs_q_eval[modelname][cul_scope_id][this.call_i].value
    )*/}</span></span>`;
    return wrap;
  }

  ignoreEvent() {
    return false;
  }
}

function workings(div, view, set_formulae_visible, setCursor, setFormula) {
  //debugger;
  return Decoration.set(
    [
      ...[...introspection.cul_links.values()] // maintain consistent order with calls_annotations, small inconsistency now x_indefined ?
      .filter(d => !d.from.includes('undefined')).filter((d) => d.reason == "call").map((d,i) => ({...d, i})).filter(d => div.scope == +d.to.split("_")[0]) // bugs for double digit scopes
    .map((d) =>
        Decoration.widget({
          widget: new AnswersWorkingsOverlayWidget(div, d.i, d, d.i, "call", set_formulae_visible, setCursor, setFormula),
          side: 1
        }).range(
          d.loc.start.index +
            d.from.length -
            0 - Math.floor(d.from.length/2) // this split is wrong for interest_rate for example!
        )
      )
    ]
  );
}


function answers(div, view) {
  return Decoration.set(
    [
      ...[...introspection.cul_functions.values()].filter(d => d.reason == 'definition' || d.reason == 'definition (renamed)').map((d,i) => ({...d, i})).filter(d => d.cul_scope_id == div.scope).map((d) =>
        Decoration.widget({
          widget: new AnswersWorkingsOverlayWidget(div, d.i, d, d.i, "definition"),
          side: 1
        }).range(
          //                    }).range(view.state.doc.line(e).from)

          view.state.doc.line(d.loc.start.line).to
          //d.from.length -
          /* todo take midpoint start-end */
        )
      )
    ] /* todo take midpoint start-end */
  );
}


const overlay_answers =  (div) => [
  // prevent input events coming from the contenteditable div that
  // propagates and triggers a cell refresh; we shall handle it ourselves.
  ViewPlugin.fromClass(
    class {
      constructor(view) {
        this.decorations = answers(div, view);
      }
      update(update) {
        // updates cause difficulty clicking numbers - thankfully not needed
        if (update.docChanged/* || update.viewportChanged*/)
          this.decorations = answers(div, update.view);

        // not necessary for read-only
      }
    },
    {
      decorations: (v) => v.decorations,
      eventHandlers: {}
    }
  )]

const overlay_workings = (div, set_formulae_visible, setCursor, setFormula) => [
  // prevent input events coming from the contenteditable div that
  // propagates and triggers a cell refresh; we shall handle it ourselves.
  ViewPlugin.fromClass(
    class {
      constructor(view) {
        this.decorations = workings(div, view, set_formulae_visible, setCursor, setFormula);
      }

      update(update) {

        if (update.docChanged/* || update.viewportChanged*/)
        this.decorations = workings(div, update.view, set_formulae_visible, setCursor, setFormula);

        // updates cause difficulty clicking numbers - thankfully not needed
        // not necessary for read-only
        //if (update.docChanged || update.viewportChanged)
         //this.decorations = workings(update.view);
      }
    },
    {
      decorations: (v) => v.decorations
    }
  )

]




//////////////////

let i = 0;

export const v = ({doc, div, update, updateSelection, set_formulae_visible, set_hover, setCursor, setFormula, parent}) => new EditorView({
  doc,
  extensions: [
    javascript(),

    EditorState.readOnly.of(readonly),

    overlay_workings(div, set_formulae_visible, setCursor, setFormula),
    overlay_answers(div),

    //EditorView.editable.of(!readonly), // leaving editable so that code can be searched; but removes cursor which is nice

    // basicSetup

    //lineNumbers(),   // make optional? Actually keeping due to space used by Framework gutter selection - review this? mini mode no gutter/line nums? or for Mobile?
    //highlightActiveLineGutter(),
    highlightSpecialChars(), // I can put custom stuff here
    highlightTrailingWhitespace(), // me. trailing whitespace is buggy e.g. after if (Math.abs(x()) > radius()) return 0;
    history(),
    foldGutter(),
    lintGutter(),
    drawSelection(),
    dropCursor(),
    EditorState.allowMultipleSelections.of(true), // float this
    indentOnInput(),
    syntaxHighlighting(defaultHighlightStyle, { fallback: true }),
    bracketMatching(),
    closeBrackets(),
    autocompletion(),
    //rectangularSelection(),
    //crosshairCursor(),
    //readonly ? EditorState.readOnly.of(readonly) : highlightActiveLine(),
    highlightSelectionMatches(),
    keymap.of([
      ...closeBracketsKeymap,
      ...defaultKeymap, // ctrl + i select parent syntax
      ...searchKeymap,
      ...historyKeymap,
      ...foldKeymap, // ctrl+ alt + [ or ] fold/unfold all
      ...completionKeymap, // ctrl+space
      ...lintKeymap // ctrl+shift+m
    ]),

    // a maybe one
    //scrollPastEnd(),

    EditorView.lineWrapping,
    EditorView.theme({
      "&": {
        height: "40vh",
        background: "white",
        marginTop: "10px",
        fontSize: "1.2rem",
      },
      ".cm-scroller": { overflow: "auto" }
    }),

    indentationMarkers({
      colors: {
        light: 'LightBlue',
        dark: 'DarkBlue',
        activeLight: 'LightGreen',
        activeDark: 'DarkGreen',
      }
    }),

    // TEMP OFF Replaced by Answers cursorTooltip(),


    // TODO add cul snippets! & capture tabs proper
    javascriptLanguage.data.of({
      autocomplete: snippetbuilder({
        source: jssnippetarray
      })
    }),

    // BUG this runs even whenever readonly !
    interact({
      rules: [
        // bool toggler
        {
          regexp: /true|false/g,
          cursor: 'pointer',
          onClick: (text, setText) => {
            switch (text) {
              case 'true': return setText('false');
              case 'false': return setText('true');
            }
          },
        },

        // a rule for a number dragger
        {
          // the regexp matching the value
          regexp: /-?\b\d+\.?\d*\b/g,
          // set cursor to "ew-resize" on hover
          cursor: "ew-resize",
          // change number value based on mouse X movement on drag
          onDrag: (text, setText, e) => {
            const newVal = Number(text) + e.movementX;
            if (isNaN(newVal)) return;
            setText(newVal.toString());
          },
        }
      ],
    }),

    !decorations ? EditorState.readOnly.of(readonly) : calculang_identifier_decorations(set_formulae_visible, set_hover, div),

    // plugin 1
    formulaDecorations(div, set_formulae_visible),


    // from https://codemirror.net/docs/guide/
    ViewPlugin.fromClass(class {
      constructor(view) {
        this.dom = view.dom.appendChild(document.createElement("div"))
        this.dom.style.cssText =
          "position: absolute; inset-block-start: 2px; inset-inline-end: 5px; font-size: 0.2em"
        this.dom.textContent = view.state.doc.length
      }

      update(update) {
        if (update.docChanged)
          this.dom.textContent = update.state.doc.length
      }

      destroy() { this.dom.remove() }
    }),

    // DELETE?
    EditorView.updateListener.of(u => {
      console.log('UPDATE HAPPENED')
      
      /*if (!i++) foldAll(u.view);*/ console.log('hihi', u.docChanged); let new_s = u.state.doc.toString();
      if (u.docChanged) { /*update(u)*/; console.log('DN update listener plugin NOT !!! called an update') }; }),

    // Toph Tuckers eval-in-place implementation
    // https://observablehq.com/@tophtucker/eval-in-place
    keymap.of([
      {
        key: "F8", // clashes with breakpoints when devtools open
        run: expand
      }
    ]),
    keymap.of([
      {
        key: "F9",
        run: expand_calculang(updateSelection)
      }
    ]),
    keymap.of([
      {
        key: "ctrl-e",
        run: expand_calculang(updateSelection)
      }
    ]),

    linter(
      view => {
        let o = esLint(new eslint.Linter(), config)(view).filter(d => !(d.source == 'eslint:no-undef' && d.message.includes("_in'")));
        if (o.length == 0) {
          //div.fs = ({...div.fs, [div.filename]: view.state.doc.toString() })
          update(view); // also update on drag events for inspect plugin?
          
        }
        // TODO also put introspection update in linter pass (or o/w debounced)?
        return o;
      }),

  ],
  parent: parent
})

let editor = async ({ doc, update, updateSelection, set_formulae_visible, set_hover, setCursor, setFormula }) => {

  const div = document.createElement('div')

  div.filename = 'entrypoint.cul.js'
  div.scope = 0



  div.setFS = async (d) => {
    if (1) {
      div.scope = 0
      div.fs = ({...d});//({"entrypoint.cul.js": d})
      // recompile
      // update a, update ab
      // ensure plugins use new introspection details
    introspection = div.introspection = await getIntrospection("entrypoint.cul.js", div.fs)
      aa.dispatch(//aa.state.update(
        {
          changes: { from: 0, to: aa.state.doc.length, insert: div.fs['entrypoint.cul.js'] }
        }
      )
    }

    //await div.compile()
  }

  div.setScope = d => {
    if (div.scope == d) return;
    div.scope = d
    div.filename = [...div.introspection.cul_scope_ids_to_resource.values()][div.scope].split("?")[0]
    document.getElementById('filename').textContent = div.filename
    // scope not relevant except in output/mid-processing document.getElementById('scope').textContent = 'scope ' + div.scope
    aa.dispatch(//aa.state.update(
      {
        changes: { from: 0, to: aa.state.doc.length, insert: div.fs[div.filename] }
      }
    )
    console.log('filename', div.filename)

    //updateOriginal(div.fs[div.filename])

 


  }

  div.setFS({...div.fs, 'entrypoint.cul.js': doc})
  introspection = div.introspection = await getIntrospection("entrypoint.cul.js", div.fs)
  div.id = 'c'
  //div.fs = ({'entrypoint.cul.js': doc})
  const divA = document.createElement('div')
  divA.id = 'divA'
  divA.appendChild(html`<u><b><i id="filename">entrypoint.cul.js`)
  const dd = document.createElement('div')
  divA.appendChild(dd)

  const aa = v({ parent: dd, div, doc, update, updateSelection, set_formulae_visible, set_hover, setCursor, setFormula });

  div.appendChild(divA)
  //document.getElementById('editor_placeholder').appendChild(div)
  //parent.appendChild(div)



  return div;
}

// Toph Tuckers eval-in-place implementation
// https://observablehq.com/@tophtucker/eval-in-place

const expand = function (view) {
  console.log("expand", view)
  view.dispatch(
    view.state.changeByRange((range) => {
      const result = evaluate(view.state.sliceDoc(range.from, range.to));

      // Evaluation errored, don’t do anything
      if (result === false) return { range };

      // How much longer is the new string?
      const rangeLengthDif = result.length - (range.to - range.from);

      return {
        changes: {
          from: range.from,
          to: range.to,
          insert: result
        },
        // The updated selection range
        range: EditorSelection.range(
          range.from,
          range.to + rangeLengthDif
        )
      };
    })
  );

  return true;
}

// based on expand above
const expand_calculang = updateSelection => function (view) {
  //debugger;
  console.log("expand calculang", view)
  view.dispatch(
    view.state.changeByRange((range) => {
      const result = evaluate(view.state.sliceDoc(range.from, range.to));

      updateSelection({/*range.from*/from: {line:view.state.doc.lineAt(view.state.selection.main.from).number,
        column:view.state.selection.ranges[0].from - view.state.doc.lineAt(view.state.selection.main.from).from}, 
        to:{line:view.state.doc.lineAt(view.state.selection.main.to).number,
          column:view.state.selection.ranges[0].to - view.state.doc.lineAt(view.state.selection.main.to).from}})
      //updateSelection(view.state.sliceDoc(range.from, range.to))

      // Evaluation errored, don’t do anything
      if (result === false || 1) return { range };

      // How much longer is the new string?
      const rangeLengthDif = result.length - (range.to - range.from);

      return {
        changes: {
          from: range.from,
          to: range.to,
          insert: result
        },
        // The updated selection range
        range: EditorSelection.range(
          range.from,
          range.to + rangeLengthDif
        )
      };
    })
  );

  return true;
}

// Currently just manually passes in d3 and lodash (_) as fun things to use.
// I’d love to figure out how to pass in everything in scope at the cursor!!
const evaluate = (expr) => {
  try {
    const result = new Function(...Object.keys(libs), `return ${expr}`)(
      ...Object.values(libs)
    );
    return typeof result === "object" ? JSON.stringify(result) : String(result);
  } catch {
    return false;
  }
}

// Add anything you want F9 to be able to reference during evaluation;
// these are passed to the new Function constructed above.
// DN todo: add calculang model somehow => F9 breaks from inputs (but still terse)
const libs = ({
  //d3,
  //_
})


export { editor }

//export { EditorView, basicSetup, javascript }