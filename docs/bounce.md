---
title: Bouncing Ball ⛹️‍♂️ (Scrolltorial)
toc: false
---

scroll the main page for a narrative

scroll the main page for a narrative

scroll the main page for a narrative

scroll the main page for a narrative

<details open><summary>notes</summary>

- I do want reactivity for formula changes though

</details>


```js
const CONFIG = {USE_HIGHLIGHTING: true /* to show workings */}
const excludes = [] // exclusions for "reactive inputs" (under dev tools atm)
```

```js
// if cursor values not set, will lead to errors, and that can make UI super unresponsive (garbage numbers is better than errors!)
setCursor('dampener_in',dampener_in);
setCursor('dx_in',3);
setCursor('t_in',10);
```

```js
setFormula('x');
```


```js
display([v0,v1])
```

```js
const step_Input = Inputs.input(6);
const step = Generators.input(step_Input);
```

```js

view(Inputs.bind(Inputs.range([0, 6], {
  label: "step ▶️",
  step: 1,
  value: 0
}), step_Input))


```


```js
  if (step != v0) {
    console.log("RUNNING", v0, step);
    const animation = await gemini.animate(mapped[0], mapped[1], gem);
    await animation.play("#viz");
    setV0(step);
  }
```

<!-- @include: /home/declan/repos/next-calculang-gallery/TEMPLATE.md -->

<div id="content">
  
# bouncing ball ⚽⛹️‍♂️

```js
const viz_placeholder = html`<div id="viz" class="card"></div>`

display(viz_placeholder)
```

```js
const dampener_in = view(Inputs.range([-1,1.10], {label:'dampener_in', step: .0005, value: .9}))
```

```js
const spec = ({
  background: "#ffeeee",
  mark: { type: "point", filled: true, tooltip: true },
  config: { axis: { grid: false } },
  width: 400,
  height: 400,
  encoding: {
    x: {
      field: "x",
      type: "quantitative",
      scale: { domain: [80, 300] }
    },
    y: {
      field: "y",
      type: "quantitative",
      sort: "descending",
      scale: { domain: [10, 230], nice: false }
    },
    detail: { field: "t_in", type: "nominal" },
    color: { field: "t_in", type: "nominal", legend: false },
    size: { value: 180 }
  }
})
```

```js
// interactivity via vega signals and listeners
const viz = embed('#viz', mapped[0], {renderer:'svg'})
```


```js
/*
viz.view.data("source", JSON.parse(`[{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":0,"x":103,"y":50},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":1,"x":106,"y":53},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":2,"x":109,"y":58.7},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":3,"x":112,"y":66.83},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":4,"x":115,"y":77.14699999999999},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":5,"x":118,"y":89.4323},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":6,"x":121,"y":103.48907},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":7,"x":124,"y":119.140163},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":8,"x":127,"y":136.22614670000002},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":9,"x":130,"y":154.60353203000003},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":10,"x":133,"y":174.14317882700004},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":11,"x":136,"y":190},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":12,"x":139,"y":169.4143178827},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":13,"x":142,"y":153.88720397713},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":14,"x":145,"y":142.91280146211702},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":15,"x":148,"y":136.03583919860532},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":16,"x":151,"y":132.84657316144478},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":17,"x":154,"y":132.9762337280003},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":18,"x":157,"y":136.09292823790025},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":19,"x":160,"y":141.89795329681021},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":20,"x":163,"y":150.1224758498292},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":21,"x":166,"y":160.52454614754626},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":22,"x":169,"y":172.88640941549164},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":23,"x":172,"y":190},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":24,"x":175,"y":175.87432305884917},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":25,"x":178,"y":166.1612138118134},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":26,"x":181,"y":160.41941548948122},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":27,"x":184,"y":158.25179699938226},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":28,"x":187,"y":159.3009403582932},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":29,"x":190,"y":163.24516938131302},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":30,"x":193,"y":169.79497550203087},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":31,"x":196,"y":178.68980101067694},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":32,"x":199,"y":190},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":33,"x":202,"y":178.99465704221853},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":34,"x":205,"y":172.08984838021522},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":35,"x":208,"y":168.87552058441221},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":36,"x":211,"y":168.98262556818952},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":37,"x":214,"y":172.0790200535891},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":38,"x":217,"y":177.86577509044872},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":39,"x":220,"y":190},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":40,"x":223,"y":181.79192046682635},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":41,"x":226,"y":177.40464888697005},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":42,"x":229,"y":176.45610446509937},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":43,"x":232,"y":178.60241448541578},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":44,"x":235,"y":183.53409350370055},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":45,"x":238,"y":190},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":46,"x":241,"y":182.56148888354372},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":47,"x":244,"y":178.86682887873306},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":48,"x":247,"y":178.54163487440346},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":49,"x":250,"y":181.2489602705068},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":50,"x":253,"y":190},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":51,"x":256,"y":184.56340714350696},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":52,"x":259,"y":182.67047357266324},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":53,"x":262,"y":183.9668333589039},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":54,"x":265,"y":190},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":55,"x":268,"y":190},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":56,"x":271,"y":190},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":57,"x":274,"y":190},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":58,"x":277,"y":190},{"dampener_in":0.9,"dx_in":3,"model_id":0,"input_cursor_id":0,"t_in":59,"x":280,"y":190}]`
))*////*.resize()*/.run(); // turn off resize
```


```js
const data_source = calcudata({ // projection1
  models: [model],
  outputs: ["x", "y"],
  input_domains: { t_in: _.range(0, 60) },
  input_cursors: [cursor],
  pivot: true
});

const projection0 = calcudata({
  models: [model0],
  outputs: ["x", "y"],
  input_domains: { t_in: _.range(0, 60) },
  input_cursors: [cursor],
  pivot: true
});

display(data_source)
```


```js
const fs0 = ({
  "entrypoint.cul.js": await (
    await fetch(
      `https://models-on-a-plane.pages.dev/stories/bounce/bounce-${v0}.cul.js`
    )
  ).text()
})

const introspection0 = await getIntrospection('entrypoint.cul.js', fs0)

const compiled0 = await compile_new('entrypoint.cul.js', fs0, introspection0) // ?
const esm0 = compiled0[0]
//display(introspection.cul_input_map) todo put under devtools
//display(introspection)

const bundle0 = bundleIntoOne(compiled0, introspection0, true)


const u0 = URL.createObjectURL(new Blob([bundle0], { type: "text/javascript" }))
console.log(`creating ${u0}`)

const model0 = await import(u0)


//display(Object.assign(document.createElement('div'), {className: 'hide-empty-block'}))

invalidation.then(() => {console.log(`revoking ${u0}`); URL.revokeObjectURL(u0)});

```




```js
display(spec0)
```



</div>

</div>



<div class="step" style="margin-top:200px; height:60vh">
<div style="position: sticky; top:30vh; pointer-events: none">
<div class="message">

we start with a ball at a fixed point @ (100,50) ⚽

</div>
</div>
</div>

<div class="step" style="margin-top:200px; height:60vh">
<div style="position: sticky; top:30vh; pointer-events: none">
<div class="message">

add x velocity ➡️

</div>
</div>
</div>

<div class="step" style="margin-top:200px; height:60vh; padding-bottom:100vh">
<div style="position: sticky; top:30vh; pointer-events: none">
<div class="message">

add y velocity ↘️

</div>
</div>
</div>

<div class="step" style="margin-top:200px; height:60vh; padding-bottom:100vh">
<div style="position: sticky; top:30vh; pointer-events: none">
<div class="message">

bounce off floor ⛹️‍♂️

</div>
</div>
</div>

<div class="step" style="margin-top:200px; height:60vh; padding-bottom:100vh">
<div style="position: sticky; top:30vh; pointer-events: none">
<div class="message">

add y acceleration 🏎️ 

</div>
</div>
</div>

<div class="step" style="margin-top:200px; height:60vh; padding-bottom:100vh">
<div style="position: sticky; top:30vh; pointer-events: none">
<div class="message">

floor alignment 🔧

</div>
</div>
</div>

<div class="step" style="margin-top:200px; height:60vh; padding-bottom:100vh">
<div style="position: sticky; top:30vh; pointer-events: none">
<div class="message">

dampen y energy 🏆

</div>
</div>
</div>

</div>


<div style="margin-top:100px"></div>

## Risk

To visualize risk - to do anything with risk, it's important to consider **many scenarios**: not just one (and not even just one exercise).

To visualize risk - to do anything with risk, it's important to consider **many scenarios**: not just one (and not even just one exercise).

To visualize risk - to do anything with risk, it's important to consider **many scenarios**: not just one (and not even just one exercise).

To visualize risk - to do anything with risk, it's important to consider **many scenarios**: not just one (and not even just one exercise).

To visualize risk - to do anything with risk, it's important to consider **many scenarios**: not just one (and not even just one exercise).

To visualize risk - to do anything with risk, it's important to consider **many scenarios**: not just one (and not even just one exercise).

```js
import {FileAttachment} from "npm:@observablehq/stdlib";

//const v0 = 6

const cul_default = ({
  "entrypoint.cul.js": await (
    await fetch(
      `https://models-on-a-plane.pages.dev/stories/bounce/bounce-${v1}.cul.js`
    )
  ).text()
})
```


```js
// I CAN'T USE reactive width because hot reload will wipe it to 0
// https://github.com/observablehq/framework/issues/1194
const content_width = Generators.width(document.getElementById("content2")); // keep as a generator for reactivity

// circular definition if I use cul_default ?!
const cul_Input = Inputs.input(cul_default);
const cul = Generators.input(cul_Input);
```



<style>
.sticky {
  height: 80vh; position: sticky; top: 10%;
}

.message {
  text-align:center;
  background: rgba(50,50,50,0.3);
  width: 50%;
  margin: auto;
  pointer-events:auto;
  padding:20px; border: 3px solid purple; border-radius: 0;
  /*width: */ /* to set a smaller width maybe recycle quarto theming stuff that does adaptive width? */
  box-shadow: /*12px 12px 2px 1px rgba(0, 0, 255, .2);*/ 0 0 18px 10px rgba(200, 0, 255, .2);
}

.message p {
  background: lightgrey;
  font-size: 2em
}

body {height: 100vh}
</style>

```js
/*setSpec0(
  Object.assign(
    {}, spec, { data: { values: data_source } }
  )
)*/
1
```
```js
const spec0 = Mutable({})

function setSpec0() {
  spec0.value = Object.assign({}, spec, { data: { values: data_source }});
  return spec0.value
}


```


```js
import {require} from "npm:d3-require";

const gemini = await require("https://cdn.jsdelivr.net/gh/declann/gemini@1c2e679/gemini.web.js");

console.log('GEMINI', gemini)

const mapped = [Object.assign({}, spec, { data: { values: projection0 } }), Object.assign({}, spec, { data: { values: data_source } })].map(gemini.vl2vg4gemini)

const gem = ({
  //"meta": {"name": "All at once"},
  timeline: {
    sync: [
      /*{
            "component": {"axis": "x"},
            "timing": {"duration": {"ratio": 1}}
          },
          {
            "component": {"axis": "y"},
            "timing": {"duration": {"ratio": 1}}
          },*/
      {
        component: { mark: "marks" },
        change: { data: ["t_in"] },
        timing: { duration: { ratio: 1 } }
      }
    ]
  },
  totalDuration: 600
})
```

```js
1//const animation = await gemini.animate(mapped[0], mapped[1], gem);
```
```js
import scrollama from "npm:scrollama";

const scroller = scrollama()

// https://github.com/russellsamora/scrollama/issues/145#issuecomment-1764582014
//debounce = require("throttle-debounce").debounce //
//window.onresize = () => {mutable inhibit.value = true; scroller.resize(); mutable inhibit.value = false}//debounce(100, scroller.resize())


scroller.setup({
  progress: false,
  step: '.step',
  debug: false//true
})
.onStepEnter(async response => {
  console.log('onStepEnter', response, step_Input.value)

  step_Input.value = response.index; step_Input.dispatchEvent(new Event("input", {bubbles: true}))

    //input.dispatchEvent(new Event("input", {bubbles: true}));


  //console.log('chk', projection0, data_source)

  //const o = setSpec0()

  //console.log('before', [v0,v1])
  // 

  // here: spec0 is current
  //const o = Object.assign({}, spec0)
  //setV1(response.index) // will trigger setSpec0
    //await a();


  //setSpec0()

  //console.log('chk', o)

  //console.log('after', [v0,v1])
})

invalidation.then(() => scroller.destroy()); // seems really wrong ?!?! but this is also what I'd do if I use own intersection observer ??? https://observablehq.observablehq.cloud/framework-example-intersection-observer/


```

```js
const v0 = Mutable(6)
//const v1 = Mutable(0)

function setV0(v) {
  v0.value = v
}
/*function setV1(v) {
  v1.value = v
}*/
```

```js
const v1 = step
```

