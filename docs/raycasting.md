---
title: Raycasting 🔫
toc: false
---

<!-- CLICK ON A NUMBER NEEDED TO SET CURSOR E.G. CLICK TO MAP_LOOKUP, Hover/step change should do it? -->
<!-- x should depend on path, i.e. be set during navigation. diff for step in H or V ray -->
<!-- rvv rvh -> map_lookup -->
<!-- DID HACKY FIX IN set_step_in-- >

<!-- latest, except not using highlighting
or data_source pattern (try C&P from fosdem)

consolidate setFormula and formulae_visible ?
 -->

 ```js
/*Inputs.button([
  ["expand all", () => {formulae_visible.value = [...formulae_all]}],
  ["collapse all", () => {formulae_visible.value = []}],
])*/ // Mutable update only works in same cell!
 ```


```js
//display(cursor)
//display(hover)
//display(frame_val)
//display(keys_stream_function_in())

const CONFIG = {USE_HIGHLIGHTING: true /* to show workings */}

// needed (empty ok)
const excludes = ["keys_stream_function_in", "f_in", "fisheye_correction_in", "keys_stream_version_in", "ray_angle_in", "step_in", "x_in", "y_in"]//["duration_in", "year_in", "annual_payment_in", "interest_rate_in"]
```

```js
const keys_stream_function_in = (a) => [...keys_shared]
//debugger;
const player_angle_in = model.player_angle({keys_stream_function_in,
keys_stream_version_in, f_in})

//display(player_angle_in)

```

```js
//debugger //
const highlights =  calls_annotations.map(d => ({...d, /*ambiguous (from/to better)*/ formula:d.to.split('_')[1]}))
    .filter(d => d.formula == formula)
```

```js
/*viewof player_x_in = Inputs.input(1.7)
viewof player_y_in = Inputs.input(5.5)
viewof ray_angle_in = Inputs.input(999)
viewof fisheye_correction_in = Inputs.input(true)
viewof step_in = Inputs.input(0)
viewof x_in = Inputs.input(3.5)
viewof y_in = Inputs.input(3.5)
viewof f_in = Inputs.input(0)
viewof key_stream_version_in = Inputs.input(0)
viewof keys_stream_version_in = Inputs.input("hi")*/
```


```js
setCursor('player_x_in',player_x_in);
```
```js
setCursor('player_y_in',player_y_in);
```
```js
setCursor('ray_angle_in',ray_angle_in);
```
```js
setCursor('fisheye_correction_in',fisheye_correction_in);
```

```js
setCursor('step_in',step_in);
```

```js
setCursor('x_in',x_in);
```

```js
setCursor('y_in',y_in);
```

```js
setCursor('f_in',f_in);
```

```js
setCursor('key_stream_version_in',key_stream_version_in);
```


```js
setCursor('keys_stream_version_in',keys_stream_version_in);
```


```js
setCursor('keys_stream_function_in',/*keys_stream_function_in*/ (a) => [...keys_shared]); // ??????????
```


<style>
  button {
    //transform: scaleX(2) scaleY(2);
    font-size: 60px;
    width: 80px;
    height: 50px;
  }
  #raycasting2 svg {
  background: linear-gradient(to bottom, lightblue, pink, white);
  }
  #reflection svg {
    margin-top: -10px;
  background: linear-gradient(to bottom, orange, pink, white);

    /*filter: blur(10px);
      background: white; linear-gradient(to bottom, lightblue, green, white);*/
      transform: scaleY(-0.7); /*translateY(65px);*/
      mask: linear-gradient(to bottom, transparent, black 98%);
  }

</style>

```js
// not currently used: setFormula('balance');
```

<!-- @include: /home/declan/repos/next-calculang-gallery/TEMPLATE.md -->

<div id="content">
  
# Raycasting 🔫

```js

// https://observablehq.com/d/867e1424e1d093ab


//const frame_val = Mutable(0)
//const keys_shared = Mutable([]) // CIRCULARITY: EVERY f_in THIS RESETS

// fn ifyed, alt?
function key_stream_fn() { // https://observablehq.com/d/867e1424e1d093ab
  let frame = 0
  let e = html`<div class='target' style='color:pink; background:#eee' tabIndex=1>Keyboard target</div>`;
  let keys = [];
  e.value = keys;
  let gap = new Date();
  e.addEventListener("keyup", (evt) => {
    console.log('keys_shared', keys_shared)
    //debugger;
    keys.push({
      key: evt.key,
      delay: new Date() - gap,
      frame: keys.length+1
    });
    keys_shared_Input.value = [...keys_shared_Input.value, {
      key: evt.key,
      delay: new Date() - gap,
      frame: keys_shared_Input.value.length+1
    }];
    keys_shared_Input.dispatchEvent(new CustomEvent("input"));

    gap = new Date();
    e.dispatchEvent(new CustomEvent("input"));
    evt.preventDefault(); // for revealjs? not enough
//debugger;
set_f_in(f_in+1)
    //f_in.value = f_in.value + 1;
    //f_in.dispatchEvent(new CustomEvent("input"));

    

          /// TODO set(viewof f_in, viewof f_in.value+1)

  });

  return e;
}

// when I use this, will prob break replaying after time travel
//display(key_stream_fn()) // disabled keyboard target for demo!

```


```js
const viz_placeholder = html`<div id="raycasting2" class="card" style="margin-bottom:-50px"></div>`

display(viz_placeholder)
```

<details open><summary>🕹️</summary>

```js
function press(key) {
  keys_shared_Input.value = [...keys_shared_Input.value.filter((d) => d.frame <= f_in), {
      key,
      frame: f_in+1
    }];
    keys_shared_Input.dispatchEvent(new CustomEvent("input"));
    set_f_in(f_in+1)
    //set_show_ray(false)
    if (f_in != keys_shared_Input.value.length) {
      keys_stream_version_in_Input.value = keys_stream_version_in_Input.value + '!'
      keys_stream_version_in_Input.dispatchEvent(new CustomEvent("input"));
    }

      //ray_angle_in_Input.value = 999
  //ray_angle_in_Input.dispatchEvent(new CustomEvent("input"));

    
    //set_show_ray_v(false)
    //set_show_ray_h(false)

}

view(Inputs.button([["⬆️ U", () => {press('ArrowUp')}],
["⬇️ D", () => {press('ArrowDown')}],
]))


view(Inputs.button([
["⬅️ L", () => {press('ArrowLeft')}],
["➡️ R", () => {press('ArrowRight')}]]))
```
</details>

```js
const level_placeholder = html`<details><summary style="font-size: 2em; margin-bottom:10px">🗺️</summary>
<div id="level"></div></details>`

display(level_placeholder)
```

```js
const scene_spec = ({
  config: {autosize: 'fit'},
    "data": {"name": "source"},

  datasets: {'source': [] },
  width: 1000, //"container",
  height: 550*scene_size,
  //height: 400,
  //height: "container",
  //background: "linear-gradient(blue, pink)",
  mark: { type: "bar", clip: true },
  //transform: [{calculate: "2*datum.r_hit_v+(0)", as: "r_hit_v__r_hit_h_or_v"}],
  encoding: {
    x: {
      field: "ray_angle_in",
      type: "ordinal",
      axis: guides ? { format: ",.2f", grid: false } : null,
      sort: "descending"
    },
    y: {
      field: "ray_inverse_distance",
      type: "quantitative",
      scale: { domain: [0, 1] },
      axis: guides ? { grid: false } : null
    },
    stroke: {value: 'black'},
    strokeOpacity: {      condition: { param: "ray_angle_in", value: 1 },
      value: 0.1,
},
    /*strokeWidth: {
      condition: { param: "ray_angle_in", value: 2 },
      value: 1,
    },*/
    color: {
      condition: { param: "ray_angle_in", value: "red" },
      //"datum": {
        //"value": "1",
      //},
      scale: {domain: [2,3,4,5,6,7],range: ["hsl(250, 76%, 60%)","hsl(250, 76%, 72%)","hsl(200, 76%, 60%)","hsl(200, 76%, 72%)","hsl(100, 76%, 60%)","hsl(100, 76%, 72%)"]},
      field: "r_hit_v__r_hit_h_or_v",
      //type: "nominal",
      legend: guides
    },
    opacity: {
      field: 'ray_distance',
      legend: guides,
      scale: {domain: [0,15], range: [1,.1]},
      type: 'quantitative'
    }
  },
  params: [
    {
      name: "player_angle_in",
      value: { player_angle_in: 0 },
      select: {
        type: "point",
        //on: "mousemove{0,50}",
        on: "[touchdown, touchup] > touchmove, [mousedown, mouseup] > mousemove", //{10, 100}",
        //        on: "[mousedown, mouseup] > mousemove",
        nearest: true,
        toggle: false,
        clear: false,
        encodings: ["x"]
      }
    },

    {
      name: "ray_angle_in",
      value: { ray_angle_in: 0 },
      select: {
        type: "point",
        //on: "mousemove{0,50}",
        on: "mousemove", //{10, 100}",
        //        on: "[mousedown, mouseup] > mousemove",
        nearest: true,
        toggle: false,
        clear: false,
        encodings: ["x"]
      }
    },

    {
      name: "formula",
      //value: { ray_angle_in: 0 },
      select: {
        type: "point",
        //on: "mousemove{0,50}",
        on: "mousedown", //{10, 100}",
        //        on: "[mousedown, mouseup] > mousemove",
        nearest: true,
        toggle: false,
        clear: false,
        encodings: ["y"]
      }
    }
  ]
})


// interactivity via vega signals and listeners
const scene = embed('#raycasting2', scene_spec, {renderer:'svg', actions})
```
```js

//debugger;
scene.view.addSignalListener("ray_angle_in", (a, b) => {
  //debugger;
  set_show_ray(true);
  ray_angle_in_Input.value = b.ray_angle_in[0]
  ray_angle_in_Input.dispatchEvent(new CustomEvent("input"));


  //mutable show_ray = true;
  //set(viewof ray_angle_in, b.ray_angle_in[0])
})



scene.view.addSignalListener("formula", (a, b) => {
  //debugger;
  set_formulae_visible(["ray_inverse_distance"])
  //set(viewof formulae_visible, ["ray_inverse_distance"])
  //set(viewof year_in, b.year_in[0])
  document.querySelector('.calculang_f_'+"ray_inverse_distance").scrollIntoView(scrollIntoViewOpts)
 //}
})

```

```js
const scene_data = calcuvegadata({
  $schema: "https://vega.github.io/schema/vega-lite/v5.json",
  models: [model],
  spec: ({...scene_spec, a: {field: 'r_hit_v', b: {field:'r_hit_h_or_v'}}}),
  domains: {
    //step_in: _.range(0, 8.1, 1),
    ray_angle_in: _.range(-1 / 2, 1.01 / 2, ray_angle_step_size).map((d) => d + player_angle_in) //[ray_angle_in, ray_angle_in + 0.4, ray_angle_in - 0.4]
  },
  input_cursors: [
    {
      fisheye_correction_in,
keys_stream_function_in,
keys_stream_version_in, f_in,
//player_angle_in /* needed for fisheye */
    }
  ]
}).map(d => ({...d, r_hit_v__r_hit_h_or_v: 2*d.r_hit_v+(d.r_hit_h_or_v == 'h' ? 1 : 0)}))

scene.view.data("source", scene_data).run(); // turn off resize
```

```js
const level_data = calcuvegadata({
  models: [model],////////
  spec: rays,
  domains: {
    step_in: _.range(0, 8.1, 1),
    ray_angle_in: [ray_angle_in, ],//_.range(-1 / 2, 1.01 / 2, /*0.025*20 ROUNDING CAUSES BUG TO NOT SHOW*/).map((d) => d + ray_angle_in) //[ray_angle_in, ray_angle_in + 0.4, ray_angle_in - 0.4]
  },
  input_cursors: [{       keys_stream_function_in,
keys_stream_version_in, f_in, fisheye_correction_in
//player_angle0_in//: ray_angle_in
}]
})

/*display(keys_stream_function_in())
display(cursor)
display(level_data)
display(ray_angle_in)*/

if (ray_angle_in != null)
level.view.data("source", level_data).run(); // turn off resize
```

```js
const data_source = [];/*calcuvegadata({
  models: [model],
  spec,
  domains: {
    formula: [],//formulae_not_inputs,
    //year_in: _.range(0,cursor.duration_in+0.1),
  },
  input_cursors: [
    cursor
  ]
})*/
```

```js
const map_data = _.range(0.5,8).map(x_in => _.range(0.5,8).map(y_in => ({x_in, y_in, map_lookup: model.map_lookup({x_in,y_in})}))).flat()

level.view.data("map_data", map_data)/*.resize()*/.run(); // turn off resize
```

```js
function set_step_in(v) {
  step_in_Input.value = v
  step_in_Input.dispatchEvent(new CustomEvent("input"));
  if (show_ray_h) {
    x_in_Input.value = model.rhx({f_in, keys_stream_function_in, keys_stream_version_in, ray_angle_in, step_in: v})
    x_in_Input.dispatchEvent(new CustomEvent("input"));
    y_in_Input.value = model.rhy({f_in, keys_stream_function_in, keys_stream_version_in, ray_angle_in, step_in: v})
    y_in_Input.dispatchEvent(new CustomEvent("input"));
  }
  if (show_ray_v) {
    x_in_Input.value = model.rvx({f_in, keys_stream_function_in, keys_stream_version_in, ray_angle_in, step_in: v})
    x_in_Input.dispatchEvent(new CustomEvent("input"));
    y_in_Input.value = model.rvy({f_in, keys_stream_function_in, keys_stream_version_in, ray_angle_in, step_in: v})
    y_in_Input.dispatchEvent(new CustomEvent("input"));
  }
};
```

```js
level.view.addSignalListener('step_in', (_,b) => {
  set_step_in(b[0])
  //setCursor('step_in', b[0]) // why is this needed?
  //set(viewof step_in, b[0])
  // TODO process the formula part [1]
  console.log(b);
})



level.view.addSignalListener("rhv", (a, b) => {
  set_step_in(b)
  set_formulae_visible(["rhv"])
  //set(viewof formulae_visible, ["rhv"])
  //set(viewof step_in, b)
  document.querySelector('.calculang_f_'+"rhv").scrollIntoView(scrollIntoViewOpts)
 //}
})

level.view.addSignalListener("rvv", (a, b) => {
  set_step_in(b)
  set_formulae_visible(["rvv"])
  //set(viewof formulae_visible, ["rvv"])
  //set(viewof step_in, b)
  document.querySelector('.calculang_f_'+"rvv").scrollIntoView(scrollIntoViewOpts)
 //}
})

```

```js
const player_data =  calcuvegadata({
  models: [model],
  spec: player_spec,
  domains: {
  },
  input_cursors: [{
    keys_stream_function_in,
    keys_stream_version_in,
    f_in, fisheye_correction_in
  }]
})

level.view.data("player_data", player_data).run();
```

```js
const fov_cursor =  ({    keys_stream_function_in,
    keys_stream_version_in,
    f_in,
})

const fov_data = [{id:0, x:model.player_x(fov_cursor), y:model.player_y(fov_cursor)},{id:1, x:model.rvx({...fov_cursor, step_in: 10, ray_angle_in: player_angle_in-.5}), y:model.rvy({...fov_cursor, step_in: 10, ray_angle_in: player_angle_in-.5})},{id:1, x:model.rvx({...fov_cursor, step_in: 10, ray_angle_in: player_angle_in+.5}), y:model.rvy({...fov_cursor, step_in: 10, ray_angle_in: player_angle_in+.5})}]


level.view.data("fov", fov_data).run();
```

```js
const rays_hv_data =  calcuvegadata({
  models: [model],
  spec:      [{field: 'rv_hit_x'}, {field: 'rv_hit_y'},{field: 'rh_hit_x'}, {field: 'rh_hit_y'},   {field: 'player_x'}, {field: 'player_y'}]
,
  domains: {
    //step_in: _.range(0,8.1)
  },
  input_cursors: [{
    keys_stream_function_in,
    keys_stream_version_in,
    f_in,
    ray_angle_in, fisheye_correction_in
  }]
})

level.view.data("rays_hv_data", rays_hv_data).run();
```


```js


const rays =  [
{
      from: { data: "source" },
      type: "symbol",
      encode: {
        enter: {
          x: { field: "r_hit_x", scale: "x" },
          y: { field: "r_hit_y", scale: "y" },
          size: { field: "ray_distance", scale: "length" },
          fill: { value: "orange" },
          //opacity: {value: 0.4},
          fillOpacity: {value: 0.1},
          stroke: { value: "black" },
          shape: { value: "M -0.1 -0.4 L 0.3 -0.7 L 0.2 -0.1 L 0.8 0 L 0.2 0.5 L 0.2 0.9 L -0.1 0.1 L -0.3 0.6 L -0.4 -0.6" } // made with https://yqnn.github.io/svg-path-editor/
        },
        update: {
          x: { field: "r_hit_x", scale: "x" },
          y: { field: "r_hit_y", scale: "y" },
          size: { field: "ray_distance", scale: "length" },
        }
      }

},
({
  from: { data: "source" },
  //mark: {type: "rule", clip: true},
  type: 'rule',
  encode: {
    enter: {
      stroke: { value: "red" },
      strokeWidth: { value: 3 },
      strokeOpacity: { value: 0.5 },
      strokeDash: {value: [8,8]}
    },
    update: {
      x: { field: "player_x", scale: "x" },
      y: { field: "player_y", scale: "y" },
      x2: { field: "r_hit_x", scale: "x" },
      y2: { field: "r_hit_y", scale: "y" }
      //stroke: { field: "ray_angle_in" }
    }
  }
})]



const fov_spec =  ({
  from: { data: "fov" },
  type: "line",
  encode: {
    enter: {
      stroke: { value: "#0FFF50" },
      strokeWidth: { value: 5.01 },
      strokeOpacity: { value: 1 },
      interpolate: {value: 'linear-closed'}
    },
    update: {
      x: { field: "x", scale: "x" },
      y: { field: "y", scale: "y" },
      fill: { value: "green" },
      fillOpacity: {value: 0.2}
      //x2: { field: "rvx", scale: "x" },
      //y2: { field: "rvy", scale: "y" }
      //stroke: { field: "ray_angle_in" }
    }
  }
})

const map_spec =  ({
  from: { data: "map_data" },
  type: "rect",
  encode: {
    enter: {
      x: { signal: "floor(datum.x_in)", scale: 'x' },
      x2: { signal: "floor(datum.x_in)+1-0.05", scale: 'x' },
      y: { signal: "floor(datum.y_in)" , scale: 'y'},
      y2: { signal: "floor(datum.y_in)+1-0.05", scale: 'y' },
      fill: { field: 'map_lookup', scale: 'map_lookup'}
      //color: { field: "floor(datum.y_in)+1" },

    },
  }
})


const player_spec =  ({
      // PLAYER
      from: { data: "player_data" },
      type: "symbol",
      encode: {
        enter: {
          x: { field: "player_x", scale: "x" },
          y: { field: "player_y", scale: "y" },
          size: { value: 500 },
          fill: { value: "yellow" },
          stroke: { value: "black" },
          shape: { value: "arrow" }
        },
        update: {
          x: { field: "player_x", scale: "x" },
          y: { field: "player_y", scale: "y" },
          angle: { signal: "-180 - 90 - player_angle_in * 180 / PI" }
        }
      }
    })

    


const ray_steps_spec =  ([
{
      // HORIZONTAL REDS
      from: { data: "ray_steps_data" },
      name: 'h',
      type: "symbol",
      encode: {
        enter: {
          //opacity: { value: opacity_vertical },
          //opacity: { field: "if_ray_hit_horizontal", scale: "hit" },
          x: { field: "rhx", scale: "x" },
          y: { field: "rhy", scale: "y" },
          size: { value: 300 },
          fill: { value: "red" },
          shape: { value: "circle" },
          strokeWidth: {value: 5},
        },
        update: {
          stroke: { signal: "(datum.step_in == step_in[0] && step_in[1] == 'h') ? 'yellow' : null"},//, scale: "hit" },
          x: { field: "rhx", scale: "x" },
          y: { field: "rhy", scale: "y" }
        }
      }
    },
    {
      // VERTICAL GREENS
      from: { data: "ray_steps_data" },
      type: "symbol",
      name: 'v',
      encode: {
        enter: {
          //opacity: { value: opacity_vertical },
          //opacity: { field: "if_ray_hit_vertical", scale: "hit" },
          x: { field: "rvx", scale: "x" },
          y: { field: "rvy", scale: "y" },
          size: { value: 200 },
          fill: { value: "green" },
          shape: { value: "circle" },
          strokeWidth: {value: 5},

        },
        update: {
          //opacity: { value: opacity_vertical },
          stroke: { signal: "(datum.step_in == step_in[0] && step_in[1] == 'v') ? 'yellow' : null"},//, scale: "hit" },
          x: { field: "rvx", scale: "x" },
          y: { field: "rvy", scale: "y" }
        }
      }
    }])



const ray_v_spec =  [
{
      from: { data: "rays_hv_data" },
      type: "symbol",
      encode: {
        enter: {
          x: { field: "rv_hit_x", scale: "x" },
          y: { field: "rv_hit_y", scale: "y" },
          size: { field: "ray_distance", scale: "length" },
          fill: { value: "green" },
          //opacity: {value: 0.4},
          fillOpacity: {value: 1},
          stroke: { value: "black" },
          shape: { value: "circle" }
        },
        update: {
          x: { field: "rv_hit_x", scale: "x" },
          y: { field: "rv_hit_y", scale: "y" },
          size: { field: "ray_distance", scale: "length" },
        }
      }

},
({
  from: { data: "rays_hv_data" },
  //mark: {type: "rule", clip: true},
  type: 'rule',
  encode: {
    enter: {
      stroke: { value: "green" },
      strokeWidth: { value: 10 },
      strokeOpacity: { value: 1 },
      //strokeDash: {value: [8,8]}
    },
    update: {
      x: { signal: "datum.player_x+0.05", scale: "x" },
      y: { signal: "datum.player_y+0.05", scale: "y" },
      x2: { signal: "datum.rv_hit_x+0.05", scale: "x" }, // calcuvegadata doesn't process signal expressions!
      y2: { signal: "datum.rv_hit_y+0.05", scale: "y" },
      fake: {field: 'rv_hit_x'}, fake2: {field: 'rv_hit_y'},      fake3: {field: 'player_x'}, fake4: {field: 'player_y'}


      /*x: { signal: "datum.player_x+1", scale: "x" },
      y: { signal: "datum.player_y+0.5", scale: "y" },
      x2: { signal: "datum.rv_hit_x+0.5", scale: "x" },
      y2: { signal: "datum.rv_hit_y+0.5", scale: "y" }*/
      //stroke: { field: "ray_angle_in" }
    }
  }
})]

const ray_h_spec =  [
{
      from: { data: "rays_hv_data" },
      type: "symbol",
      encode: {
        enter: {
          x: { field: "rh_hit_x", scale: "x" },
          y: { field: "rh_hit_y", scale: "y" },
          size: { field: "ray_distance", scale: "length" },
          fill: { value: "orange" },
          //opacity: {value: 0.4},
          fillOpacity: {value: 1},
          stroke: { value: "black" },
          shape: { value: "circle" }
        },
        update: {
          x: { field: "rh_hit_x", scale: "x" },
          y: { field: "rh_hit_y", scale: "y" },
          size: { field: "ray_distance", scale: "length" },
        }
      }

},({
  from: { data: "rays_hv_data" },
  //mark: {type: "rule", clip: true},
  type: 'rule',
  encode: {
    enter: {
      stroke: { value: "orange" },
      strokeWidth: { value: 10 },
      strokeOpacity: { value: 1 },
      //strokeDash: {value: [8,8]}
    },
    update: {
      x: { signal: "datum.player_x-.05", scale: "x" },
      y: { signal: "datum.player_y-.05", scale: "y" },
      x2: { signal: "datum.rh_hit_x-.05", scale: "x" },
      y2: { signal: "datum.rh_hit_y-.05", scale: "y" },
            fake: {field: 'rh_hit_x'}, fake2: {field: 'rh_hit_y'}

      //stroke: { field: "ray_angle_in" }
    }
  }
}),

]



const level_spec = ({
  data: [
    { name: "source", values: [] }, // layer 5 for rays by step
    { name: "fov", values: [] },
    { name: "map_data", values: [] },
    { name: "player_data", values: [] },
    { name: "hits_data", values: [] },
    { name: "ray_steps_data", values: [] },
    { name: "ray_h_data", values: [] },
    { name: "ray_v_data", values: [] },
    { name: "rays_hv_data", values: [] },
  ],
  //background: "grey",
  //width: 'container',
  width: 400*1.4,
  height: 300*1.4-40,
  background: '#ccc',
  signals: [
    { name: "player_angle_in" },


    {
      "name": "step_in",
      value: [-10,'?'],
      "on": [{"events": "@h2:mouseover", "update": "[datum.step_in]"},{"events": "@v2:mouseover", "update": "[datum.step_in]",}]
    },
    {
      "name": "rhv",
      value: -10,
      "on": [{"events": "@h2:click", "update": "datum.step_in"}]
    },
    {
      "name": "rvv",
      value: -10,
      "on": [{"events": "@v2:click", "update": "datum.step_in"}]
    }
  ],
  scales: [
    { name: "x", type: "linear", domain: [0, 8], range: "width" },
    { name: "y", type: "linear", domain: [0, 8], range: "height", reverse: true },
    { name: "hit", type: "ordinal", domain: [0, 1], range: [0.01, 0.1] },
    { name: "length", type: "linear", domain: [2, 8], range: [1000, 10]  },
    { name: "length2", type: "linear", domain: [2, 6], range: [1000, 0]  },
    { name: "map_lookup", type: "ordinal", domain: [0, 1, 2, 3], range: ["white", "hsl(250, 76%, 60%)","hsl(200, 76%, 60%)","hsl(100, 76%, 60%)"] }
  ],
  marks: [
    map_spec,

    fov_spec,
    ...(show_ray && show_ray_h ? ray_h_spec : []),
    ...(show_ray && show_ray_v ? ray_v_spec : []),
    ...(show_ray ? rays : []),
    player_spec,


...(show_ray && show_ray_h ? [{
      // HORIZONTAL REDS
      from: { data: "ray_h_data" },
      name: 'h2',
      type: "symbol",
      encode: {
        enter: {
          opacity: { value: 0.1 },
          //opacity: { field: "if_ray_hit_horizontal", scale: "hit" },
          x: { signal: "datum.rhx-0.05*0", scale: "x" },
          y: { signal: "datum.rhy-0.05*0", scale: "y" },
          size: { value: 600 },
          //fill: { value: "red" },
          angle: {value: 45},
          shape: { value: "cross" },
          stroke: { value: "black"},
          strokeWidth: {value: 2},
        },
        update: {
          fill: { signal: "(datum.step_in == step_in[0]) ? 'pink' : null"},//, scale: "hit" },
          opacity: { signal: "(datum.step_in == step_in[0]) ? 1 : 0.1"},//, scale: "hit" },
          //stroke: { signal: "(datum.step_in == step_in[0]) ? 'black' : null"},//, scale: "hit" },
          x: { signal: "datum.rhx-0.05*0", scale: "x" },
          y: { signal: "datum.rhy-0.05*0", scale: "y" },
        }
      }
    }] : []),

...(show_ray && show_ray_v ? [{
      // VERTICAL GREENS
      from: { data: "ray_v_data" },
      name: 'v2',
      type: "symbol",
      encode: {
        enter: {
          opacity: { value: 0.1 },
          //opacity: { field: "if_ray_hit_horizontal", scale: "hit" },
          x: { signal: "datum.rvx+0.05*0", scale: "x" },
          y: { signal: "datum.rvy+0.05*0", scale: "y" },
          size: { value: 600 },
          //fill: { value: "red" },
          angle: {value: 45},
          shape: { value: "cross" },
          stroke: { value: "black"},
          strokeWidth: {value: 2},
        },
        update: {
          fill: { signal: "(datum.step_in == step_in[0]) ? 'pink' : null"},//, scale: "hit" },
          opacity: { signal: "(datum.step_in == step_in[0]) ? 1 : 0.1"},//, scale: "hit" },
          //stroke: { signal: "(datum.step_in == step_in[0]) ? 'black' : null"},//, scale: "hit" },
          x: { signal: "datum.rvx+0.05*0", scale: "x" },
          y: { signal: "datum.rvy+0.05*0", scale: "y" },
        }
      }
    }] : [])
  ],
  axes: [
    { orient: "top", scale: "x", labelFontSize: 20, labelColor: 'blue'},
    { orient: "left", scale: "y", labelFontSize: 20, labelColor: 'blue'}
  ],
})


const level = embed('#level', level_spec, {renderer: 'svg', actions})
```

```js
const c =  ({keys_stream_function_in,
    keys_stream_version_in,
    f_in,
    ray_angle_in, fisheye_correction_in /* irrelevant but.. */})



const ray_h_data = _.range(0,model.rh_hit_step({...c})+0.1).map(step_in => ({
  rhx: model.rhx({...c, step_in}),
  rhy: model.rhy({...c, step_in}),
  step_in, fisheye_correction_in
}))


const ray_v_data = _.range(0,model.rv_hit_step({...c})+0.1).map(step_in => ({
  rvx: model.rvx({...c, step_in}),
  rvy: model.rvy({...c, step_in}),
  step_in, fisheye_correction_in
}))

level.view.data('ray_h_data', ray_h_data)
level.view.data('ray_v_data', ray_v_data)
```

```js
level.view.signal('player_angle_in', player_angle_in).run();
```


```js
const show_ray = Mutable(false)

function set_show_ray(v) {
  show_ray.value = v;
}
```

```js
const show_ray_v = Mutable(false)

function set_show_ray_v(v) {
  show_ray_v.value = v;
}
```

```js
// coordination
if (hover == 'rv_hit_distance') {
  set_show_ray_v(true)
  set_show_ray_h(false)
} 
if (hover == 'rh_hit_distance') {
  set_show_ray_v(false)
  set_show_ray_h(true)
} 
```

```js
const show_ray_h = Mutable(false)

function set_show_ray_h(v) {
  show_ray_h.value = v;
}
```



<details class="card"><summary>🤫</summary>

```js
const guides = view(Inputs.toggle({ label: "guides", value: false }))
const ray_angle_step_size = view(Inputs.select([0.02,0.01,0.005], {label: 'step size (resolution)', value: 0.01}))
const scene_size = view(Inputs.range([0.1,1], {value:0.6, label: 'size'}))

```


```js

const f_in_Input = Inputs.input(0);
const f_in = Generators.input(f_in_Input);

```

```js

const keys_stream_version_in_Input = Inputs.input(0);
const keys_stream_version_in = Generators.input(keys_stream_version_in_Input);

```
```js

const keys_shared_Input = Inputs.input([]);
const keys_shared = Generators.input(keys_shared_Input);

```
```js

const ray_angle_in_Input = Inputs.input(999);
const ray_angle_in = Generators.input(ray_angle_in_Input);

```
```js
f_in;

      set_show_ray(false)

      set_show_ray_v(false)
    set_show_ray_h(false)

```

```js

function set_f_in(v) {
  //debugger;
  f_in_Input.value = f_in_Input.value + 1;
  f_in_Input.dispatchEvent(new CustomEvent("input"));
}

```

${view(Inputs.bind(Inputs.range([0, 200], {label:'time travel 🔙', step:1, value: 0}), f_in_Input))}

```js
const fisheye_correction_in = view(Inputs.toggle({label:'🐟👀 correct', value: true}))
//const f_in =  view(Inputs.range([0, 200], {label:'time travel 🔙', step:1, value: 0}))
```

```js
const step_in_Input = Inputs.range([0, 10], {label:'step_in', step:1, value: 0})
const step_in = view(step_in_Input)
```

```js

const player_x_in_Input = Inputs.input(1.7)
const player_x_in = Generators.input(player_x_in_Input);
const player_y_in_Input = Inputs.input(5.5)
const player_y_in = Generators.input(player_y_in_Input);
```

```js
//const ray_angle_in = Inputs.input(999)
const x_in_Input = Inputs.input(3.5)
const x_in = Generators.input(x_in_Input);

const y_in_Input = Inputs.input(3.5)
const y_in = Generators.input(y_in_Input);
```

```js

const key_stream_version_in_Input = Inputs.input(0)
const key_stream_version_in = Generators.input(key_stream_version_in_Input);

//const keys_stream_version_in_Input = Inputs.input("hi") // redundant ?!?!
//const keys_stream_version_in = Generators.input(keys_stream_version_in_Input);

```

</details>
</div>

</div></div><!-- close tag started in template 2x -->


```js
import {FileAttachment} from "npm:@observablehq/stdlib";

const cul_default = ({'entrypoint.cul.js': await FileAttachment('./cul/raycasting.cul.js').text() })
//https://cdn.jsdelivr.net/gh/declann/calculang-miscellaneous-models@main/models/cashflows/simple-cfs.cul.js
```


```js
// I CAN'T USE reactive width because hot reload will wipe it to 0
// https://github.com/observablehq/framework/issues/1194
const content_width = Generators.width(document.getElementById("content2")); // keep as a generator for reactivity

// circular definition if I use cul_default ?!
const cul_Input = Inputs.input(cul_default);
const cul = Generators.input(cul_Input);
```
