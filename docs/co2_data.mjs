import { filter/*, extent*/,dsv } from 'https://esm.run/d3'

const co2_data_raw = await dsv(",", 'https://next-calculang-gallery.netlify.app/annual-co2-emissions-per-country.csv')
//const co2_data_raw = await (await fetch ('http://127.0.0.1:8082/annual-co2-emissions-per-country.csv')).text() // 'csv-loader!./annual-co2-emissions-per-country.csv';


export const co2_data = (year) => {
  return filter(co2_data_raw, d => { /*console.log(d);*/ return d['Entity'] == 'World' })
    .filter(d => +d['Year'] == year).map(d => +d['Annual CO₂ emissions'])[0]
}