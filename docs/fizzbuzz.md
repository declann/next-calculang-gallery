---
title: Fizz Buzz
toc: false
---

```js
const CONFIG = {USE_HIGHLIGHTING: true /* to show workings */}
// needed (empty ok)
const excludes = [] // exclusions for "reactive inputs" (under dev tools atm)
```

```js
setCursor('i_in',i_in);
```

```js
setFormula('fizzbuzz');
set_formulae_visible(['fizzbuzz'])
```

<!-- @include: /home/declan/repos/next-calculang-gallery/TEMPLATE.md -->

<div id="content">
  
# Fizz Buzz

💡 *Click/drag to see & highlight workings*

```js
const viz_placeholder = html`<div id="viz" class="card"></div>`

display(viz_placeholder)
```

```js
const i_in = view(Inputs.range([-1,30], {label:'i_in', step: 1, value: 5}))
```


```js
//debugger //
const highlights =  calls_annotations.map(d => ({...d, /*ambiguous (from/to better)*/ formula:d.to.split('_')[1]}))
    .filter(d => d.formula == formula)
```



```js

function highlight(d) {
  let ans = false
  highlights.forEach(e => {
    //Object.keys()
    if (e.cursor.i_in == d.i_in && e.from.split('_')[1] == d.formula) // loop through domains? or all keys? (alternatives surely better) Only limited things mapped to visual
      ans = true
  })
  return ans
}

// + current row ?
function current(d) {
  let ans = false
  if (d.formula == formula && d.i_in == cursor.i_in)
    ans = true
  return ans
}

const data_source_with_highlights = data_source.map(
  d => ({...d, highlight: highlight(d), current: current(d)})
)
.map(d => ({...d, value: d.value == '' ? '-' : d.value}))

```


```js echo
const spec = ({
  // vega-lite
    encoding: {
    x: { field: 'formula', type: 'nominal', "axis": {"labelAngle": -30, "orient": "top", title:null, "labelLimit": 300, labelFontSize: 16} },
    y: { field: 'i_in'/*, axis: {titleFontSize:20, labelFontSize: 30, } */ },
    color: {field: 'value', legend: false},
    text: {field: 'value', /*, format:',.2f'*/},
    size: {value: 12}
  },
  layer: [

    {
      mark: {type:'text', fontWeight:'bold', dx:2, dy:2},
      transform: [{filter: "datum.highlight"}],
      encoding: {
            size: {value: 15},
        color: {value:'yellow'}
      }
    },
    {
      mark: {type:'text', tooltip:false, from: 'data'},
            //transform: [{filter: "!datum.current"}], // messes selection; better out or using conditional size
      encoding: {
        size: { value: 16, condition: {test: 'datum.current', value: 1} }
      },
        params: [
    {
      name: "formula",
      //value: { ray_angle_in: 0 },
      select: {
        type: "point",
        //on: "mousemove{0,50}",
        on: "[touchdown, touchup] > touchmove, mouseup, touchup, [mousedown, mouseup] > mousemove",
        //on: "[touchdown, touchup] > touchmove, [mousedown, mouseup] > mousemove", //{10, 100}",
        //        on: "[mousedown, mouseup] > mousemove",
        nearest: true,
        toggle: false,
        clear: false,
        encodings: ["x", "y"]
      }
    }
  ]

    },
    {
      mark: {type:'text', fontWeight:'bold', dx:0, dy:0},
      transform: [{filter: "datum.current"}],
      encoding: {
        color: {value:'black'},
        size: {value: 20}
      }
    },
  ],

  data: { name: "data" },
  autosize: { "type": "fit", "contains": "padding"},
  // no reactive w/h due to https://github.com/observablehq/framework/issues/1194
  width: 450,//Math.min(500,content_width-30),//Math.min(400,content_width),
  height: 600,//Math.min(500,content_width-30)/1.2,//Math.min(400,content_width-30),
  background:'rgba(0,0,0,0)',
})

// interactivity via vega signals and listeners
const viz = embed('#viz', spec)
```


```js echo
// is this data_source_with_highlights data flow ok ... ?
viz.view.data("data", data_source_with_highlights).resize().run(); // turn off resize
```


```js echo
const data_source = calcuvegadata({
  models: [model],
  spec,
  domains: {
    formula: formulae_not_inputs,
    i_in: _.range(1,31),
  },
  input_cursors: [
    cursor
  ]
})
```

```js echo
viz.view.addSignalListener("formula", (a, b) => {
  // no modularity here:
  //const newScope = +(fns_fromDefinition.find(d => (d.cul_scope_id == 0 && d.name == b.formula[0])).fromDefinition.split('_')[0])
  //if (newScope !== editor.scope)
    //editor.setScope(+(fns_fromDefinition.find(d => (d.cul_scope_id == 0 && d.name == b.formula[0])).fromDefinition.split('_')[0]))
  setCursor('i_in', b.i_in[0])
  setFormula(b.formula[0])
  set_formulae_visible([b.formula[0]])
  document.querySelector('.calculang_f_'+b.formula[0]).scrollIntoView(scrollIntoViewOpts)
});
```

</div>

</div><!-- close tag started in template -->


```js
import {FileAttachment} from "npm:@observablehq/stdlib";

const cul_default = ({

'entrypoint.cul.js':

`export const fizz = () => (i() % 3 == 0 ? 'Fizz' : '');
export const buzz = () => (i() % 5 == 0 ? 'Buzz' : '');

export const fizzbuzz = () => {
  if (fizz() + buzz() != '')
    return fizz() + buzz()
  else return i()
};

// inputs:
export const i = () => i_in;
`
})
//https://cdn.jsdelivr.net/gh/declann/calculang-miscellaneous-models@main/models/cashflows/simple-cfs.cul.js
```


```js
// I CAN'T USE reactive width because hot reload will wipe it to 0
// https://github.com/observablehq/framework/issues/1194
const content_width = Generators.width(document.getElementById("content2")); // keep as a generator for reactivity

// circular definition if I use cul_default ?!
const cul_Input = Inputs.input(cul_default);
const cul = Generators.input(cul_Input);
```
