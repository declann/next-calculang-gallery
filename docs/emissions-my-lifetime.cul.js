// based on data from https://ourworldindata.org/grapher/annual-co2-emissions-per-country saved on 29/1/24
// and additional assumptions

// data is:
/* Global Carbon Budget (2023) – with major processing by Our World in Data. “Annual CO₂ emissions – GCB” [dataset]. Global Carbon Project, “Global Carbon Budget” [original data]. Retrieved January 29, 2024 from https://ourworldindata.org/grapher/annual-co2-emissions-per-country */

// Disclaimers:
/*
The assumptions, methodology and limitations of a model should be carefully considered for any purpose you apply it to.

I haven’t noted these here, or otherwise documented example models. Proceed with caution 🙏
*/

//import { co2_data } from "http://127.0.0.1:3000/_file/co2_data.mjs";
import { co2_data } from "https://next-calculang-gallery.netlify.app/co2_data.mjs";
import {
  differenceInDays,
  differenceInHours,
  min,
  max,
  isBefore,
  isEqual,
  getDayOfYear,
} from 'https://esm.run/date-fns';
import _ from 'https://esm.run/underscore';

export const age = () => age_in;
export const now = () => now_in;
export const timing = () => timing_in; // "start of birthyear", "end of birthyear": optional inclusion of birthyear emissions, even though it's hard to interpret given old emission values less significant!

export const birthyear = () => now().getFullYear() - age();

export const emissions_year = () => emissions_year_in;

export const emissions_values = () => {
  if (emissions_year() < 1750) console.error("year < 1750");
  if (emissions_year() <= 2022) return co2_data(emissions_year());
  // assumes 2022 rate of emissions for 2023 onwards:
  else return emissions({ emissions_year_in: 2022 });
};

export const emissions = () => {
  //if (isBefore(new Date(year(), 0, 1), now())) return emissions_values(); // edges?
  if (emissions_year() < now().getFullYear()) return emissions_values(); // edges?
  if (emissions_year() > now().getFullYear()) return 0; // edges?
  else return (emissions_values() / 365) * getDayOfYear(now()); // leap years missing, date specifics
  // every hour a different result !
};

export const total_emissions = () =>
  _.range(1750, now().getFullYear() + 0.1).reduce(
    (acc, emissions_year_in) => acc + emissions(),
    0
  );

export const emissions_strictly_after_birthyear = () =>
  _.range(birthyear() + 1, now().getFullYear() + 0.1).reduce(
    (acc, emissions_year_in) => acc + emissions(),
    0
  );

export const emissions_my_lifetime = () =>
  emissions_strictly_after_birthyear() +
  (timing() == "start of birthyear" ? emissions({ emissions_year_in: birthyear() }) : 0);

export const emissions_my_lifetime_proportion = () =>
  emissions_my_lifetime() / total_emissions();
