
# Audience

This `next-calculang-gallery` is intended to demo and **show actual calculang code to developers**, i.e. not dumbed-down, not detached from it's "raw" syntax. It's intended to replace my Examples on [calculang.dev](https://calculang.dev), and will include model outputs (visualizations) also.

There is more code in the UI bits behind this thing than in the calculang compiler itself - and many more bugs. It's a bit of a mess, but:

**calculang is not this thing** - calculang is not technically opinionated about how it's used. calculang has compiler/introspection APIs which this uses, and there can be other tools and UIs that do particular things better.

Although it might look like it, **this thing is not a developer environment.** It's useful for quick edits, to quickly explore changes, to *actively illustrate* some calculang or modelling concepts: and this is what it's intended for, but editing is generally dangerous. For example, it's easy to cause an infinite loop and break the main thread. I can fix this, but it would still be a poor choice for other reasons, for reasonable development tasks.

## Goals (current)

- to make calculang formulas navigable, approachable, for developers
- to demo some example calculang models and outputs (usually visualizations)
- (wip) to (actively) illustrate calculang concepts: input inference, modularity, ♻️ formulas
- to show one presentation of SOME THINGS that become easy through having a pure-functional language for calculations; not exhaustive:
  - showing workings for calculations (computers should do this!)
  - strong connection visuals-formulas-workings
  - in-context editing

## Purpose (future)

- perhaps more about active development for modelling
- perhaps more about [sharing numbers](https://calculang-at-fosdem.pages.dev/slides-revealjs#/imaginary-paper) (linked to workings)
- perhaps an effort directed at [calculang issue #5 (browser extension)](https://github.com/calculang/calculang/issues/5), to reveal things about numbers on the web

## visuals-formulas-workings connection

For a video explainer on Raycasting using the example here, [check my YouTube channel](https://www.youtube.com/watch?v=hKVXRACCnqU).

## Running locally

This runs using [Observable Framework](https://observablehq.com/framework/). A `TEMPLATE.md` file is included through using a MarkdownIt plugin. I get this working using an absolute file reference to `TEMPLATE.md`. So, to run this on your machine, **you will need to update these references by searching `TEMPLATE.md` in `docs`**.

---

For more about calculang, see [calculang.dev](https://calculang.dev/).
